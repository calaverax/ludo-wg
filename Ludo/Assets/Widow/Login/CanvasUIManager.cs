﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CanvasUIManager : MonoBehaviour
{
    public static CanvasUIManager instance;

    public List<PanelBase> panels;
   // public PanelSplash splash;
    //public PanelSelectGame selectGame;
    //public PanelIngameSinglePlayer ingameSinglePlayer;
    //public PanelMultiplayerEndGame multiplayerEndGame;
    //public PanelPause panelPause;
    //public PanelContinue PanelContinue;
    public PanelLogin Login;

	// Use this for initialization
	void Start () 
    {
        panels = new List<PanelBase> { Login };

	    foreach (PanelBase panelBase in panels)
	    {
	        panelBase.gameObject.SetActive(false);
	    }
      //  splash.Show(true);
	    instance = this;
    }

    public static CanvasUIManager Get()
    {
        return instance;
    }

    public void HideAll()
    {
        foreach (PanelBase panelBase in panels)
        {
            panelBase.gameObject.SetActive(false);
        }
    }

}
