﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JatulModule;

public class LoginHandler : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        SuscribeToLogInManager();
#if UNIWEBVIEW
        UniWebviewManager.instance.onAuthSuccessfullWebView += OnLoginWithClub;
#endif
    }

    public void LogOut()
    {
        LoginManager.instance.LogOutFB();
        LoginManager.instance.LogOutJDS();
#if UNIWEBVIEW
        UniWebviewManager.instance.LogOutFS();
        #endif
    }

    private void OnLoginWithClub(User user, string code)
    {
        SetLoggedUser(user, user.hasActiveSubscription);
    }

    private void OnAutoLogIn(User user)
    {
        Debug.Log("I ENTERED AUTOLOGIN ");
        SetLoggedUser(user, user.hasActiveSubscription);
    }

    private void OnLogOut()
    {
        SetNotLogged();
    }

    private void OnJDSLogInSuccess(User user, string code)
    {
        SetLoggedUser(user, user.hasActiveSubscription);
    }
    private void OnJDSRegisterSuccess(User user, string code)
    {
        SetLoggedUser(user, user.hasActiveSubscription);
    }

    private void SetLoggedUser(User user, bool isPremium)
    {
        /*
        GameManager.instance.loggedPremiumUser = user.hasActiveSubscription;
        ChangeLogoSprite(isPremium ? sprite_premium_logo : sprite_common_logo);
        loginButton.gameObject.SetActive(false);
        loggedIn.SetActive(true);
        loginClubButton.gameObject.SetActive(false);
        playerName.text = user.nickName;
        ImageDownloader.GetImageByUrl(user.profilePicturePath, playerAvatar);
        Debug.Log("THIS IS THE USER PICTURE PATH " + user.profilePicturePath + "END PATH");
        PlayerPrefs.SetString("PremiumUser", isPremium ? "true" : "false");
        PlayerPrefs.Save();
        */
    }

    private void SetNotLogged()
    {
        /*
        loginButton.gameObject.SetActive(true);
        loggedIn.SetActive(false);
        playerName.text = "";
        playerAvatar.sprite = img_defaultAvatar;

        // if Premium, Deslogear, pero no mostrar club
#if PREMIUM_BUILD
        loginClubButton.gameObject.SetActive(false);
#else
        // else, desloguear y mostrar club.
        loginClubButton.gameObject.SetActive(true);
#endif
        */
    }

    private void SuscribeToLogInManager()
    {
        if (LoginManager.instance != null)
        {
            LoginManager.instance.OnJDSLogInSuccess += OnJDSLogInSuccess;
            LoginManager.instance.OnJDSRegisterSuccess += OnJDSRegisterSuccess;
            LoginManager.instance.OnAutoLogIn += OnAutoLogIn;
            LoginManager.instance.OnLogOut += OnLogOut;
        }
    }

}
