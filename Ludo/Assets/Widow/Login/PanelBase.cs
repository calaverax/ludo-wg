﻿using UnityEngine;
using System.Collections;

public class PanelBase : MonoBehaviour 
{

	public virtual void Start () 
    {
	}
	
	// Update is called once per frame
    public virtual void Update()
    {
	}

    public virtual void Show(bool b)
    {
        //TODO create a canvas group and modify alpha
        gameObject.SetActive(b);        
    }
}
