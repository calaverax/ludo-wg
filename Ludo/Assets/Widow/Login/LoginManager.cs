﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using JatulModule;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour {

    public static readonly string USERDATAPREF_KEY = "UserDataPrefs";

    private string accessToken;
    private const string siteUrl = "http://juegosdesiempre.com";

    //Facebook Events
    public delegate void OnFacebookLogInSuccessDelegate (string name, string fbId);
    public event OnFacebookLogInSuccessDelegate OnFacebookLogInSuccess;

    public delegate void OnFacebookLogInFailedDelegate ();
    public event OnFacebookLogInFailedDelegate OnFacebookLogInFailed;

    //JDS Events
    public delegate void OnJDSLogInSuccessDelegate (User user, string code);
    public event OnJDSLogInSuccessDelegate OnJDSLogInSuccess;

    public delegate void OnJDSLogInFailedDelegate (string error);
    public event OnJDSLogInFailedDelegate OnJDSLogInFailed;

    public delegate void OnJDSRegisterSuccessDelegate (User user, string code);
    public event OnJDSRegisterSuccessDelegate OnJDSRegisterSuccess;

    public delegate void OnJDSRegisterFailedDelegate (string error);
    public event OnJDSRegisterFailedDelegate OnJDSRegisterFailed;

    public delegate void OnJDSResetPasswordSuccessDelegate (string message);
    public event OnJDSResetPasswordSuccessDelegate OnJDSResetPasswordSuccess;

    public delegate void OnJDSResetPasswordFailedDelegate (string error);
    public event OnJDSResetPasswordFailedDelegate OnJDSResetPasswordFailed;

    //General Events

    public delegate void OnAutoLogInDelegate (User user);
    public event OnAutoLogInDelegate OnAutoLogIn;

    public delegate void OnLogOutDelegate ();
    public event OnLogOutDelegate OnLogOut;

    public User currentLoggedUser;

    public bool PremiumLoggedUser {
        get {
            if (currentLoggedUser != null)
                if (currentLoggedUser.hasActiveSubscription)
                    return true;
            return false;
        }
    }

    private static LoginManager _instance;
    public static LoginManager instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<LoginManager>();
                if (_instance != null && _instance.gameObject != null) {
                    DontDestroyOnLoad(_instance.gameObject);
                }
            }
            return _instance;
        }
    }

    #region Unity Methods

    void Awake () {
        SuscribeJatulEvents();
        /*
        UniWebviewManager.instance.Init();
        UniWebviewManager.instance.onAuthSuccessfullWebView += LoginCallback;
        */
    }
    /*
    void Start () {
        AutoLogIn();
    }
    */

    #endregion

    #region FB LogIn
    public void LogInFB () {
        if (!FB.IsInitialized) {
            FB.Init(OnFBInitComplete, OnHideUnity);
        } else {
            OnFBInitComplete();
        }
    }

    public void LogOutFB () {
        if (FB.IsInitialized) {
            if (FB.IsLoggedIn) {
                FB.LogOut();
            }
        }
    }

    private void OnFBInitComplete () {
        FB.LogInWithReadPermissions(new List<string>() { "email" }, FBLoginCallback);
    }

    private void OnHideUnity (bool isGameShown) {
        Debug.Log("Is game showing? " + isGameShown);
    }

    private void FBLoginCallback (ILoginResult result) {
        if (result.Error != null) {
            Debug.Log("Error Response:\n" + result.Error);
            if (OnFacebookLogInFailed != null) {
                OnFacebookLogInFailed();
            }
        } else if (!FB.IsLoggedIn) {
            Debug.Log("Login cancelled by Player");
            if (OnFacebookLogInFailed != null) {
                OnFacebookLogInFailed();
            }
        } else {
            Debug.Log("Login was successful!");
            accessToken = result.AccessToken.TokenString;
            Debug.Log("Result Raw Result: " + result.RawResult.ToString());
            FB.API("me?fields=id,name,email,gender,locale", HttpMethod.GET, FBDataLoginCallBack);
        }
    }

    private void FBDataLoginCallBack (IGraphResult result) {
        Debug.Log(result.RawResult.ToString());
        IDictionary dict = Facebook.MiniJSON.Json.Deserialize(result.RawResult) as IDictionary;
        // the fbId is our password for facebook accounts as the previously random generated pwd on JSDManager could be lost
        string fbId = dict["id"].ToString();
        string fbName = dict["name"].ToString();
        string fbEmail = dict["email"].ToString();

        foreach (var item in result.ResultDictionary) {
            Debug.Log(item.Key + " : " + item.Value);
        }

        JatulManager.userServices.AuthFacebook(fbId, accessToken, fbEmail, fbName);

        if (OnFacebookLogInSuccess != null) {
            OnFacebookLogInSuccess(fbName, fbId);
        }
        //Debug.Log(dict["url"]);
    }
    #endregion

    #region JDS

    public void LogInJDS (string email, string password) {
        JatulManager.userServices.Auth(email, password);
    }

    public void LogOutJDS () {
        PlayerPrefs.DeleteKey(USERDATAPREF_KEY);
        // PlayerPrefs.DeleteKey(UniWebviewManager.USERWEBDATA_KEY);
    }

    public void RegisterJDSUser (string email, string password) {
        User user = CreateUser(email, password);
        JatulManager.userServices.CreateUser(user);
    }

    private User CreateUser (string email, string password) {
        User user = new User();
        user.email = email;
        user.password = password;
        user.profilePicturePath = siteUrl + "/images/usr-default.png";
        user.thumbnailPicturePath = siteUrl + "/images/usr-default.png";
        if (email.Contains("@")) {
            user.nickName = user.email.Substring(0, user.email.IndexOf('@'));
        }
        user.ipAddress = Network.player.externalIP;
        return user;
    }

    #endregion

    #region General Methods

    public void LogOut () {

        LogOutFB();
        LogOutJDS();

        if (OnLogOut != null) {
            OnLogOut();
        }
    }

    public void RestorePassword (string email) {
        JatulManager.userServices.ResetPassword(email);
    }

    public void AutoLogIn () {
        if (Application.internetReachability == NetworkReachability.NotReachable) {
            return;
        }

        if (PlayerPrefs.HasKey(USERDATAPREF_KEY)) {
            Debug.Log("AUTOLOGIN WITH JDS " + PlayerPrefs.GetString(USERDATAPREF_KEY));
            OnJDSAutoLogIn(User.CreateUserFromJson(new JSONObject(PlayerPrefs.GetString(USERDATAPREF_KEY))));
            return;
        }

        if (FB.IsLoggedIn) {
            FB.Mobile.RefreshCurrentAccessToken((IAccessTokenRefreshResult result) => {
                accessToken = result.AccessToken.TokenString;
                FB.API("me?fields=id,name,email", HttpMethod.GET, FBDataLoginCallBack);
            });
            return;
        }

    }

    private void OnJDSAutoLogIn (User user) {
        if (OnAutoLogIn != null) {
            OnAutoLogIn(user);
        }

    }
    #endregion

    #region Callback Methods
    private void LoginCallback (User user, string code) {
        PlayerPrefs.SetString(USERDATAPREF_KEY, user.ConvertUserToJson().ToString());
        if (OnJDSLogInSuccess != null) {
            OnJDSLogInSuccess(user, code);

            string[] data = new string[3];

            if (FB.IsLoggedIn) {
                data[0] = "User e-mail: " + user.email;
                data[1] = "Login Code: " + code;
                if (code == "102")
                    data[2] = "Usuario Creado y Autenticado";
                else
                    data[2] = "Logged in With Facebook";
            } else {
                data[0] = "User e-mail: " + user.email;
                data[1] = "Login Code: " + code;
                data[2] = "Logged in With JDS-Services";
            }
            
            if (AnalyticsManager.instance != null) {
                AnalyticsManager.instance.SetUserId(user.id);
                AnalyticsManager.instance.SendCustomEvent("Logueo de Usuario", 0, data);
            }
        }
    }

    private void LoginFailCallback (string error) {
        if (OnJDSLogInFailed != null) {
            OnJDSLogInFailed(error);
        }
    }

    private void RegisterEmailCallback (User user, string code) {
        PlayerPrefs.SetString(USERDATAPREF_KEY, user.ConvertUserToJson().ToString());
        if (OnJDSRegisterSuccess != null) {
            OnJDSRegisterSuccess(user, code);

            string[] data = new string[3];

            data[0] = "User e-mail: " + user.email;
            data[1] = "Register Code: " + code;
            data[2] = "Register With JDS-Services";
            
            if (AnalyticsManager.instance != null)
                AnalyticsManager.instance.RegistrationComplete();
                //AnalyticsManager.instance.SendCustomEvent("Registro de Usuario", 0, data);

        }
    }

    private void RegisterEmailFailCallback (string error) {
        if (OnJDSRegisterFailed != null) {
            OnJDSRegisterFailed(error);
        }
    }

    private void ResetPasswordSuccessCallback (string message) {
        if (OnJDSResetPasswordSuccess != null) {
            OnJDSResetPasswordSuccess(message);
        }
    }

    private void SendResetPassEmailFailCallback (string error) {
        if (OnJDSResetPasswordFailed != null) {
            OnJDSResetPasswordFailed(error);
        }
    }

    #endregion

    #region Jatul Methods
    private void SuscribeJatulEvents () {
        JatulManager.userServices.onAuthSuccessfull += LoginCallback;
        JatulManager.userServices.onAuthFail += LoginFailCallback;
        JatulManager.userServices.onCreateSuccessfull += RegisterEmailCallback;
        JatulManager.userServices.onCreateFail += RegisterEmailFailCallback;
        JatulManager.userServices.onResetPasswordSuccess += ResetPasswordSuccessCallback;
        JatulManager.userServices.onResetPasswordFail += SendResetPassEmailFailCallback;
    }

    private void UnSuscribeJatulEvents () {
        JatulManager.userServices.onAuthSuccessfull -= LoginCallback;
        JatulManager.userServices.onAuthFail -= LoginFailCallback;
        JatulManager.userServices.onCreateSuccessfull -= RegisterEmailCallback;
        JatulManager.userServices.onCreateFail -= RegisterEmailFailCallback;
        JatulManager.userServices.onResetPasswordSuccess -= ResetPasswordSuccessCallback;
        JatulManager.userServices.onResetPasswordFail -= SendResetPassEmailFailCallback;
    }
    #endregion

}
