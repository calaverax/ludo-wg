﻿using System;
using System.Collections;
using System.Collections.Generic;
using JatulModule;
using UnityEngine;
using UnityEngine.UI;

public class PanelLogin : PanelBase {
    enum ErrorEventType {
        Login = 0,
        Register = 1,
        Generic = 2
    }

    enum SubPanel {
        Login = 0,          // 0 - Acceso
        Register = 1,       // 1 - Registrarse
        CloseSession = 2,   // 2 - Cerrar Sesion
        RecoverPass = 3,    // 3 - Recuperar Pass
        Error = 4           // 4 - Error
    }

    public List<GameObject> panels;

    public InputField login_EmailInputField;
    public InputField login_PasswordInputField;

    public InputField register_EmailInputField;
    public InputField register_PasswordInputField;
    public InputField register_RePasswordInputField;

    public InputField restore_RestorePassweordEmail;

    public Text resultTitle;
    public Text resultMessage;

    public GameObject wrongPasswordField;
    public GameObject passwordsDontMatch;
    public GameObject LogOutBtn;
    public GameObject LogInBtn;
    public Text playerName;
    public Image playerAvatar;
    public Sprite img_defaultAvatar;

    public GameObject vip;
    public GameObject NoConnectionPopup;

    public List<GameObject> listaBotonesBack;

    private bool loggedWithFacebook;
    private bool SubscribedToEvents = false;

    private Dictionary<string, string> loginErrorMsg;
    private Dictionary<string, string> registerErrorMsg;
    private Dictionary<string, string> genericErrorMsg;

    void Start () {
        InitializeErrorCodesMap();
        SuscribeToLogInManager();
    }

    /*
    public void OnEnable () {
        //LoginManager.instance.AutoLogIn();
        //base.Start();
    }

        */
    public void OnDisable () {
        //UnsuscribeToLogInManager();
    }
    public void OnDestroy () {
        UnsuscribeToLogInManager();
    }

    public void OpenLoginPanel () {
        if (Application.internetReachability == NetworkReachability.NotReachable) {
            NoConnectionPopup.SetActive(true);
            this.gameObject.SetActive(false);
            return;
        }
        this.gameObject.SetActive(true);
        CleanInputFields();
        OpenPanel(SubPanel.Login);
        //botones menu
        for (int i = 0; i < listaBotonesBack.Count; i++)
        {
            listaBotonesBack[i].GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    public void OpenErrorPanel (string errorTitle, string errorMsg) {
        resultTitle.text = errorTitle;
        resultMessage.text = errorMsg;

        OpenPanel(SubPanel.Error);
    }

    public void SuscribeToLogInManager () {
        if (SubscribedToEvents)
            return;
        if (LoginManager.instance != null) {
            LoginManager.instance.OnFacebookLogInSuccess += OnFacebookLogInSuccess;
            LoginManager.instance.OnFacebookLogInFailed += OnFacebookLogInFailed;
            LoginManager.instance.OnJDSLogInSuccess += OnJDSLogInSuccess;
            LoginManager.instance.OnJDSLogInFailed += OnJDSLogInFailed;
            LoginManager.instance.OnJDSRegisterSuccess += OnJDSRegisterSuccess;
            LoginManager.instance.OnJDSRegisterFailed += OnJDSRegisterFailed;
            LoginManager.instance.OnJDSResetPasswordSuccess += OnJDSResetPasswordSuccess;
            LoginManager.instance.OnJDSResetPasswordFailed += OnJDSResetPasswordFailed;
            LoginManager.instance.OnAutoLogIn += OnAutoLogIn;
            LoginManager.instance.OnLogOut += OnLogOut;
            SubscribedToEvents = true;
        }
    }

    public void UnsuscribeToLogInManager () {
        if (!SubscribedToEvents)
            return;

        if (LoginManager.instance != null) {
            LoginManager.instance.OnFacebookLogInSuccess -= OnFacebookLogInSuccess;
            LoginManager.instance.OnFacebookLogInFailed -= OnFacebookLogInFailed;

            LoginManager.instance.OnJDSLogInSuccess -= OnJDSLogInSuccess;
            LoginManager.instance.OnJDSLogInFailed -= OnJDSLogInFailed;
            LoginManager.instance.OnJDSRegisterSuccess -= OnJDSRegisterSuccess;
            LoginManager.instance.OnJDSRegisterFailed -= OnJDSRegisterFailed;
            LoginManager.instance.OnJDSResetPasswordSuccess -= OnJDSResetPasswordSuccess;
            LoginManager.instance.OnJDSResetPasswordFailed -= OnJDSResetPasswordFailed;

            LoginManager.instance.OnAutoLogIn -= OnAutoLogIn;
            LoginManager.instance.OnLogOut -= OnLogOut;
            SubscribedToEvents = false;
        }
    }

    #region RegistrationEvents

    private void OnLoginWithClub (User user, string code) {
        SetLoggedUser(user, user.hasActiveSubscription);
    }


    public void RegisterUserOnJDS () {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            NoConnectionPopup.SetActive(true);
            return;
        }
        string email = register_EmailInputField.text;
        string password = register_PasswordInputField.text;
        string rePassword = register_RePasswordInputField.text;

        if (password == rePassword && !string.IsNullOrEmpty(password)) {
            LoginManager.instance.RegisterJDSUser(email, password);
        } else {
            passwordsDontMatch.gameObject.SetActive(true);
            Invoke("HidePasswordsDontMath", 1.5f);
        }
    }

    private void HidePasswordsDontMath () {
        passwordsDontMatch.gameObject.SetActive(false);
    }


    #endregion

    #region UI_Buttons_Event

    public void Logout () {
        /*
        LoginManager.instance.LogOutFB();
        LoginManager.instance.LogOutJDS();
        UniWebviewManager.instance.LogOutFS();
        */


        LoginManager.instance.LogOut();
    }

    public void RestorePassword () {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            NoConnectionPopup.SetActive(true);
            return;
        }
        string email = restore_RestorePassweordEmail.text;

        if (!string.IsNullOrEmpty(email)) {
            LoginManager.instance.RestorePassword(email);
        }
    }

    public void LogInWithEmail () {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            NoConnectionPopup.SetActive(true);
            return;
        }
        if (!string.IsNullOrEmpty(login_EmailInputField.text) && !string.IsNullOrEmpty(login_PasswordInputField.text)) {
            LoginManager.instance.LogInJDS(login_EmailInputField.text, login_PasswordInputField.text);
            //    ShowLoadingScreen();
        }
    }

    public void LogInWithFB () {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            NoConnectionPopup.SetActive(true);
            return;
        }
        LoginManager.instance.LogInFB();
        //ShowLoadingScreen();
    }

    public void OpenRegisterPanel () {
        //wrongPasswordField.gameObject.SetActive(false);
        passwordsDontMatch.gameObject.SetActive(false);
        OpenPanel(SubPanel.Register);
    }

    public void OpenCloseSessionPanel () {
        //Debug.LogError("- Trying To Open Close Session Panel -");
        OpenPanel(SubPanel.CloseSession);
    }

    public void OpenPassRecoveryPanel () {
        OpenPanel(SubPanel.RecoverPass);
    }

    public void ClosePanel (int panel) {
        ClosePanel((SubPanel) panel);

        if (panel == 0)
            this.gameObject.SetActive(false);
        //CanvasMainMenu.Instance.Login.Show(false);
    }
    #endregion

    #region Utils

    public void OpenWebJDS() {
        string url = "http://www.jogosdesempre.com/";
        Application.OpenURL(url);
    }


    public void OpenTermsAndConditions () {
        string url = "http://www.juegosdesiempre.com/terminosycondiciones";

        // TODO: Activate Localized Link
        //if (LocalizationManager.Instance.CURRENT_LANG == "Portuguese")
        //    url = "http://www.jogosdesempre.com/terminosycondiciones";

        Application.OpenURL(url);
    }

    public void OpenPrivacyPolitics () {
        string url = "http://www.juegosdesiempre.com/privacidad";

        // TODO: Activate Localized Link
        //if (LocalizationManager.Instance.CURRENT_LANG == "Portuguese")
        //    url = "http://www.jogosdesempre.com/privacidad";

        Application.OpenURL(url);
    }

    public void Init () {
        if (LoginManager.instance.currentLoggedUser == null) {

            LogOutBtn.SetActive(false);
            //CheckFirstTimeLogin();
            InitializeErrorCodesMap();
            LoginManager.instance.AutoLogIn();
        }
    }

    private void InitializeErrorCodesMap () {
        loginErrorMsg = new Dictionary<string, string>();
        registerErrorMsg = new Dictionary<string, string>();
        genericErrorMsg = new Dictionary<string, string>();

        loginErrorMsg.Add("401", "logIn_txt_error_s_401");
        loginErrorMsg.Add("403", "logIn_txt_error_s_403");
        loginErrorMsg.Add("406", "logIn_txt_error_s_406");
        loginErrorMsg.Add("409", "logIn_txt_error_s_409");
        loginErrorMsg.Add("500", "logIn_txt_error_s_500");
        loginErrorMsg.Add("503", "logIn_txt_error_s_503");

        registerErrorMsg.Add("400", "logIn_txt_error_r_400");
        registerErrorMsg.Add("409", "logIn_txt_error_r_409");
        registerErrorMsg.Add("500", "logIn_txt_error_r_500");

        genericErrorMsg.Add("300", "logIn_txt_error_g_300");
        genericErrorMsg.Add("510", "logIn_txt_error_g_510");

    }

    private string handleErrorCode (string errorCode, ErrorEventType errorType) {
        if (errorType == ErrorEventType.Login) {
            foreach (KeyValuePair<string, string> pair in loginErrorMsg)
                if ((errorCode.Contains(pair.Key)) || (pair.Value.Contains(errorCode)))
                    return pair.Value;
        } else if (errorType == ErrorEventType.Register) {
            foreach (KeyValuePair<string, string> pair in registerErrorMsg)
                if ((errorCode.Contains(pair.Key)) || (pair.Value.Contains(errorCode)))
                    return pair.Value;
        } else if (errorType == ErrorEventType.Generic) {
            foreach (KeyValuePair<string, string> pair in genericErrorMsg)
                if ((errorCode.Contains(pair.Key)) || (pair.Value.Contains(errorCode)))
                    return pair.Value;
        }
        return "Unknow Error";
    }

    private void CleanInputFields () {
        login_EmailInputField.text = "";
        login_PasswordInputField.text = "";
        register_EmailInputField.text = "";
        register_PasswordInputField.text = "";
        register_RePasswordInputField.text = "";
        //resetPasswordEmail.text = "";
    }

    private void OpenPanel (SubPanel subPanel) {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            NoConnectionPopup.SetActive(true);
            return;
        }
        for (int i = 0; i < panels.Count; i++)
            panels[i].SetActive((int) subPanel == i);
    }

    private void OpenPanel (int subPanel) {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            NoConnectionPopup.SetActive(true);
            return;
        }
        for (int i = 0; i < panels.Count; i++)
            panels[i].SetActive(subPanel == i);
    }

    private void ClosePanel (SubPanel subPanel) {
        if (panels[(int) subPanel] != null)
            panels[(int) subPanel].SetActive(false);
        //if ((((int)subPanel) > 0) && (subPanel != SubPanel.CloseSession)) // ---> Close Panel and Dont Open Main Panel (Need to Close Login Also)
        if (((int) subPanel) > 0)
            if (panels[0] != null)
                panels[0].SetActive(true);

        //botones menu
        for (int i = 0; i < listaBotonesBack.Count; i++){
            listaBotonesBack[i].GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    private void SetNotLogged () {
        //if (LogOutBtn != null)
        LogOutBtn.SetActive(false);
        //if (LogInBtn != null)
        LogInBtn.SetActive(true);

        if (playerName != null)
            playerName.text = "";

        if (playerAvatar != null)
            playerAvatar.sprite = img_defaultAvatar;

        LoginManager.instance.currentLoggedUser = null;
        PlayerPrefs.SetString("PremiumUser", "false");
        PlayerPrefs.Save();
    }

    private void SetLoggedUser (User user, bool isPremium) {
        LoginManager.instance.currentLoggedUser = user;

        //Debug.LogError("Logged With Facebook: " + loggedWithFacebook);

        LogInBtn.SetActive(false);
        //Debug.LogError("HIDDING LOGIN BUTTON");
        LogOutBtn.SetActive(true);

        if (playerName != null)
            playerName.text = user.nickName;

        if (playerAvatar != null)
            ImageDownloader.GetImageByUrl(user.profilePicturePath, playerAvatar);

        //Debug.Log("THIS IS THE USER PICTURE PATH " + user.profilePicturePath + "END PATH");
        PlayerPrefs.SetString("PremiumUser", isPremium ? "true" : "false");
        PlayerPrefs.Save();
        //CanvasMainMenu.Instance.Login.Show(false);
        this.gameObject.SetActive(false);
    }
    #endregion


    #region EVENTS

    private void OnLogOut () {
        SetNotLogged();
        if (vip != null)
            vip.SetActive(false);
    }
    public void OnAutoLogIn (User user) {
        SetLoggedUser(user, user.hasActiveSubscription);
        isVip(user);
    }
    private void OnJDSResetPasswordFailed (string error) {
        OpenErrorPanel(LocalizationManager.Instance.GetLocalizedValue("logIn_title_error"),
                       LocalizationManager.Instance.GetLocalizedValue(handleErrorCode(error, ErrorEventType.Generic)));
    }
    private void OnJDSResetPasswordSuccess (string message) {
        OpenErrorPanel(LocalizationManager.Instance.GetLocalizedValue("Congratulations"),
                       LocalizationManager.Instance.GetLocalizedValue("MailSended"));
    }
    private void OnJDSRegisterFailed (string error) {
        OpenErrorPanel(LocalizationManager.Instance.GetLocalizedValue("logIn_title_error"),
                       LocalizationManager.Instance.GetLocalizedValue(handleErrorCode(error, ErrorEventType.Register)));
    }
    private void OnJDSRegisterSuccess (User user, string code) {
        OpenErrorPanel(LocalizationManager.Instance.GetLocalizedValue("last_register_step_title"),
               LocalizationManager.Instance.GetLocalizedValue("last_register_step_text"));

        SetLoggedUser(user, user.hasActiveSubscription);
    }
    private void OnJDSLogInFailed (string error) {
        OpenErrorPanel(LocalizationManager.Instance.GetLocalizedValue("logIn_title_error"),
                       LocalizationManager.Instance.GetLocalizedValue(handleErrorCode(error, ErrorEventType.Login)));
    }
    private void OnJDSLogInSuccess (User user, string code) {
        ClosePanel(SubPanel.Login);
        SetLoggedUser(user, user.hasActiveSubscription);
        isVip(user);
    }

    private void OnFacebookLogInFailed () {
        OpenErrorPanel(LocalizationManager.Instance.GetLocalizedValue("logIn_title_error"),
                       LocalizationManager.Instance.GetLocalizedValue("logIn_txt_error_r_500"));
    }

    private void OnFacebookLogInSuccess (string name, string fbId) {
        string imageUrl = "https://graph.facebook.com/" + fbId + "/picture?width=80&height=80";
        ImageDownloader.GetImageByUrl(imageUrl, playerAvatar);

        loggedWithFacebook = true;
    }
    #endregion

    private void isVip (User user) {
        if (user.hasActiveSubscription) {
            if (vip != null) {
                vip.SetActive(true);
                //PlayerPrefs.SetString(REMOVE_ADS, "removeAds");
                PlayerPrefs.SetString("removeAds", "removeAds");
                PlayerPrefs.SetString("PremiumUser", "true");
            }
        } else {
            vip.SetActive(false);
            // PlayerPrefs.SetString(CanvasMainMenu.Instance.REMOVE_ADS, "removeAds");
            PlayerPrefs.DeleteKey("removeAds");
            PlayerPrefs.SetString("PremiumUser", "false");
        }
    }

    public void closeNoInternet() {
        NoConnectionPopup.SetActive(false);
    }

}
