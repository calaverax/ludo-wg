﻿#if USE_VUNGLE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Widow.Ads {
	public class AdTypeVungle : AdType {
		
		protected string androidAdGameId = "59132b58466052bc5d001392";
		protected string iOSAdGameId = "59132ba63e9389b4210007a8";

		public override bool AdInit(List<string> placements) {
			isVideoRewardCallback = false;

			Vungle.onAdStartedEvent += OnAdStarted;
			Vungle.onLogEvent += OnLog;
			Vungle.onAdFinishedEvent += OnAdFinished;

			Vungle.init (androidAdGameId, iOSAdGameId);

			return true; 
		}

		public override bool ShowInterstitial() { 
			return false; 
		}

		public override bool ShowVideoReward (string placementId) {
			isVideoRewardCallback = true;
			Debug.Log ("Show video reward.");
			if (Vungle.isAdvertAvailable ()) {
				Vungle.playAd ();
				return true;
			} else {
				return false;
			}
		}

		public override bool ShowVideo(string placementId) {
			isVideoRewardCallback = false;
			Debug.Log("Show video");
			if (Vungle.isAdvertAvailable ()) {
				Vungle.playAd ();
				return true;
			} else {
				return false;
			}
		}

		public override bool ShowBanner (string placementId) {
			if (OnBannerStartedCallback != null)
				OnBannerStartedCallback (AdNetworks.Vungle);
			return false;
		}

		public override bool IsVideoAvailable(string placementId) { 
			return Vungle.isAdvertAvailable(); 
		}

		public override bool IsVideoRewardAvailable (string placementId) {
			return Vungle.isAdvertAvailable ();
		}

		public override bool IsBannerAvailable (string placementId) {
			return false;
		}

		void OnAdStarted () {
			if (isVideoRewardCallback) {
				isVideoRewardCallback = false;
				if (OnVideoRewardStartedCallback != null)
					OnVideoRewardStartedCallback (AdNetworks.Vungle);
			} else {
				if (OnVideoStartedCallback != null)
					OnVideoStartedCallback (AdNetworks.Vungle);
			}
		}

		void OnLog (string log) {
			Debug.Log ("Vungle Ads log: " + log);
		}

		void OnAdFinished (AdFinishedEventArgs adFinishedEventArgs) {
			isVideoRewardCallback = false;
			if (OnVideoRewardFinishedCallback != null)
				OnVideoRewardFinishedCallback (adFinishedEventArgs.IsCompletedView, AdNetworks.Vungle);
			if (OnVideoFinishedCallback != null)
				OnVideoFinishedCallback (adFinishedEventArgs.IsCompletedView, AdNetworks.Vungle);
		}
	}
}
#endif //USE_VUNGLE