﻿#define USE_ADCOLONY
#define USE_UNITYADS
//#define USE_VUNGLE

using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Widow.DynamicValuesPlugin;

namespace Widow.Ads {
    public class Ads : MonoBehaviour {

		public static string ADS_CONFIGURATION_JSON = "widowAdMediatorConfigurationJson";
		public static string TOTAL_PLAYED_TIME = "totalPlayedTime";
		protected static string VIDEOS = "videos";
		protected static string VIDEO_REWARDS = "videoRewards";
		protected static string BANNERS = "banners";
		
        public static Ads instance;

		protected bool disableState;
		protected Dictionary<AdNetworks, AdType> adTypes;
		protected JSONObject json;
		protected Dictionary<string, VideoAdPlacement> videoPlacements;
		protected Dictionary<string, VideoRewardAdPlacement> videoRewardPlacements;
		protected Dictionary<string, BannerAdPlacement> bannerAdPlacements;

        public bool ShouldShowAd {
            get {
                //if (Application.internetReachability == NetworkReachability.NotReachable)
                    //return false;
                if (LoginManager.instance.PremiumLoggedUser)
                    return false;
                return true;
            }
        }
        void Awake () {
            Debug.Log("Ad Instance created");
            if (instance == null) {
				instance = this;

                string temp = PlayerPrefs.GetString("widowAdMediatorConfigurationJson", string.Empty);

                if (string.IsNullOrEmpty(temp)) 
                {
#if UNITY_ANDROID
                    this.json = new JSONObject ("{\"videos\":[{\"placementId\":\"InterstitialVideo\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":1,\"requests\":8},\"initialAd\":{\"mode\":1,\"seconds\":1,\"requests\":10,\"sessions\":1},\"adNetworkPriority\":[{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"AdColony\",\"placementId\":\"vz9891e21e83574261b6\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"VideoReward\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":100,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vze8167c7156f442e1ac\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}");
#elif UNITY_IOS
				    this.json = new JSONObject ("{\"videos\":[{\"placementId\":\"InterstitialVideo\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":1,\"requests\":8},\"initialAd\":{\"mode\":1,\"seconds\":1,\"requests\":10,\"sessions\":1},\"adNetworkPriority\":[{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"AdColony\",\"placementId\":\"vz9891e21e83574261b6\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"VideoReward\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":100,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vze8167c7156f442e1ac\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}");
#endif
                } else {
                    this.json = new JSONObject(temp);
                }

                Init();
                DontDestroyOnLoad(this.gameObject);
			} else {
				Destroy (this);
			}
		}

		public void DestroyInstance () {
            Debug.Log("Ad Instance is dead");
			instance = null;
			Destroy (gameObject);
		}

		void Update () {
			PlayerPrefs.SetFloat (TOTAL_PLAYED_TIME, PlayerPrefs.HasKey (TOTAL_PLAYED_TIME) ? (PlayerPrefs.GetFloat (TOTAL_PLAYED_TIME) + Time.deltaTime) : 0f);
		}

		public bool GetAdDisableState () {
			return this.disableState;
		}

		public void SetAdDisableState (bool disableState) {
			this.disableState = disableState;
		}

		protected void Init()
        {
			this.videoPlacements = new Dictionary<string, VideoAdPlacement> ();
			this.videoRewardPlacements = new Dictionary<string, VideoRewardAdPlacement> ();
			this.bannerAdPlacements = new Dictionary<string, BannerAdPlacement> ();

			#if USE_ADCOLONY
			List<String> adColonyPlacements = new List<String> ();
			#endif

			#if USE_UNITYADS
			List<String> unityAdsPlacements = new List<String> ();
			#endif

			#if USE_VUNGLE
			List<String> vunglePlacements = new List<String> ();
			#endif

			for (int i = 0; i < this.json [VIDEOS].Count; i++) {
				VideoAdPlacement videoAdPlacement = new VideoAdPlacement (this.json [VIDEOS] [i]);
				this.videoPlacements.Add (videoAdPlacement.GetPlacementId (), videoAdPlacement);
				videoAdPlacement.GetAdNetworkPriorities ().ForEach ((AdNetworkPriority adNetworkPriority) => {
					#if USE_ADCOLONY
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.AdColony)
						adColonyPlacements.Add (adNetworkPriority.GetPlacementId());
					#endif

					#if USE_UNITYADS
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.UnityAds)
						unityAdsPlacements.Add (adNetworkPriority.GetPlacementId());
					#endif

					#if USE_VUNGLE
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.Vungle)
						vunglePlacements.Add (adNetworkPriority.GetPlacementId());
					#endif
				});
			}

			for (int i = 0; i < this.json [VIDEO_REWARDS].Count; i++) {
				VideoRewardAdPlacement videoRewardAdPlacement = new VideoRewardAdPlacement (this.json [VIDEO_REWARDS] [i]);
				this.videoRewardPlacements.Add (videoRewardAdPlacement.GetPlacementId (), videoRewardAdPlacement);
				videoRewardAdPlacement.GetAdNetworkPriorities ().ForEach ((AdNetworkPriority adNetworkPriority) => {
					#if USE_ADCOLONY
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.AdColony)
						adColonyPlacements.Add (adNetworkPriority.GetPlacementId());
					#endif

					#if USE_UNITYADS
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.UnityAds)
                        unityAdsPlacements.Add (adNetworkPriority.GetPlacementId());
#endif

#if USE_VUNGLE
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.Vungle)
                    vunglePlacements.Add (adNetworkPriority.GetPlacementId());
#endif
                });
			}

			for (int i = 0; i < this.json [BANNERS].Count; i++) {
				BannerAdPlacement bannerAdPlacement = new BannerAdPlacement (this.json [BANNERS] [i]);
				this.bannerAdPlacements.Add (bannerAdPlacement.GetPlacementId (), bannerAdPlacement);
				bannerAdPlacement.GetAdNetworkPriorities ().ForEach ((AdNetworkPriority adNetworkPriority) => {
					#if USE_ADCOLONY
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.AdColony)
						adColonyPlacements.Add (adNetworkPriority.GetPlacementId());
					#endif

					#if USE_UNITYADS
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.UnityAds)
                        unityAdsPlacements.Add (adNetworkPriority.GetPlacementId());
#endif

#if USE_VUNGLE
					if (((AdNetworks)Enum.Parse (typeof(AdNetworks), adNetworkPriority.GetAdNetwork(), true)) == AdNetworks.Vungle)
						vunglePlacements.Add (adNetworkPriority.GetPlacementId());
#endif
                });
			}

			adTypes = new Dictionary<AdNetworks, AdType>();

            #if USE_ADCOLONY
            AdType adColony = new AdTypeAdColony();
			adColony.AdInit(adColonyPlacements);
			adTypes.Add (AdNetworks.AdColony, adColony);
            #endif

			#if USE_UNITYADS
			AdType unityAds = new AdTypeUnityAds();
			unityAds.AdInit(unityAdsPlacements);
			adTypes.Add (AdNetworks.UnityAds, unityAds);
			#endif

			#if USE_VUNGLE
			AdType vungle = new AdTypeVungle();
			vungle.AdInit(vunglePlacements);
			adTypes.Add (AdNetworks.Vungle, vungle);
			#endif
        }

		public bool ShowVideo(string placementId, Action<AdNetworks> videoStartedCallback = null, Action<bool, AdNetworks> videoFinishedCallback = null) {
			if (!this.videoPlacements [placementId].MustShow () || this.disableState)
				return false;

			SetCallbackVideoStarted(videoStartedCallback);
			SetCallbackVideoFinished(videoFinishedCallback);

			if (this.videoPlacements [placementId].IsAvailable () && this.videoPlacements [placementId].MustShow()) {
				for (int i = 0; i < this.videoPlacements [placementId].GetAdNetworkPriorities().Count; i++) {
                    AdNetworks adNetwork = (AdNetworks)Enum.Parse (typeof(AdNetworks), this.videoPlacements [placementId].GetAdNetworkPriorities() [i].GetAdNetwork(), true);
                    if (this.adTypes [adNetwork].IsVideoAvailable (this.videoPlacements [placementId].GetAdNetworkPriorities() [i].GetPlacementId())) {
                        this.adTypes [adNetwork].ShowVideo (this.videoPlacements [placementId].GetAdNetworkPriorities() [i].GetPlacementId());
						return true;
					}
				}
			}
			return false;
		}
        public bool ShowVideo(string network, string placementId, Action<AdNetworks> videoStartedCallback = null, Action<bool, AdNetworks> videoFinishedCallback = null)
        {
            if (!this.videoPlacements[placementId].MustShow() || this.disableState)
                return false;

            SetCallbackVideoStarted(videoStartedCallback);
            SetCallbackVideoFinished(videoFinishedCallback);

            if (this.videoPlacements[placementId].IsAvailable() && this.videoPlacements[placementId].MustShow())
            {
                for (int i = 0; i < this.videoPlacements[placementId].GetAdNetworkPriorities().Count; i++)
                {
                    AdNetworks adNetwork = (AdNetworks)Enum.Parse(typeof(AdNetworks), network);//(AdNetworks)Enum.Parse(typeof(AdNetworks), this.videoPlacements[placementId].GetAdNetworkPriorities()[i].GetAdNetwork(), true);
                    if (this.adTypes[adNetwork].IsVideoAvailable(this.videoPlacements[placementId].GetAdNetworkPriorities()[i].GetPlacementId()))
                    {
                        
                        this.adTypes[adNetwork].ShowVideo(this.videoPlacements[placementId].GetAdNetworkPriorities()[i].GetPlacementId());
                        return true;
                    }
                    Debug.LogError(network + " IsVideoAvailable: (" + this.adTypes[adNetwork].IsVideoAvailable(this.videoPlacements[placementId].GetAdNetworkPriorities()[i].GetPlacementId()) + ")");
                }
            }
            return false;
        }

        public bool ShowVideoReward(string placementId, Action<AdNetworks> videoRewardStartedCallback = null, Action<bool, AdNetworks> videoRewardFinishedCallback = null) {
			if (!this.videoRewardPlacements [placementId].MustShow ())
				return false;

			SetCallbackVideoRewardStarted(videoRewardStartedCallback);
			SetCallbackVideoRewardFinished(videoRewardFinishedCallback);

			if (this.videoRewardPlacements [placementId].IsAvailable () && this.videoRewardPlacements [placementId].MustShow ()) {
				for (int i = 0; i < this.videoRewardPlacements [placementId].GetAdNetworkPriorities ().Count; i++) {
					AdNetworks adNetwork = (AdNetworks)Enum.Parse (typeof(AdNetworks), this.videoRewardPlacements [placementId].GetAdNetworkPriorities() [i].GetAdNetwork(), true);
					if (this.adTypes [adNetwork].IsVideoRewardAvailable (this.videoRewardPlacements [placementId].GetAdNetworkPriorities() [i].GetPlacementId())) {
						this.adTypes [adNetwork].ShowVideoReward (this.videoRewardPlacements [placementId].GetAdNetworkPriorities () [i].GetPlacementId ());
						return true;
					}
				}
			}
			return false;
        }

		public bool ShowBanner (string placementId, Action<AdNetworks> onBannerStartedCallback = null) {
			if (!this.bannerAdPlacements [placementId].MustShow () || this.disableState)
				return false;

			SetCallbackBannerStarted (onBannerStartedCallback);
			for (int i = 0; i < this.bannerAdPlacements [placementId].GetAdNetworkPriorities ().Count; i++) {
				AdNetworks adNetwork = (AdNetworks)Enum.Parse (typeof(AdNetworks), this.bannerAdPlacements [placementId].GetAdNetworkPriorities() [i].GetAdNetwork(), true);
				if (this.adTypes [adNetwork].IsBannerAvailable (this.bannerAdPlacements [placementId].GetAdNetworkPriorities() [i].GetPlacementId())) {
					this.adTypes [adNetwork].ShowVideoReward (this.bannerAdPlacements [placementId].GetAdNetworkPriorities () [i].GetPlacementId ());
					return true;
				}
			}
			return false;
		}

		protected void SetCallbackVideoStarted(Action<AdNetworks> callback) {
			foreach (KeyValuePair<AdNetworks, AdType> adTypeKeyValuePair in instance.adTypes)
				adTypeKeyValuePair.Value.SetCallbackVideoStarted (callback);
		}

        protected void SetCallbackVideoFinished(Action<bool, AdNetworks> callback) {
			foreach (KeyValuePair<AdNetworks, AdType> adTypeKeyValuePair in instance.adTypes)
				adTypeKeyValuePair.Value.SetCallbackVideoFinished (callback);
		}

        protected void SetCallbackVideoRewardStarted(Action<AdNetworks> callback) {
			foreach (KeyValuePair<AdNetworks, AdType> adTypeKeyValuePair in instance.adTypes)
				adTypeKeyValuePair.Value.SetCallbackVideoRewardStarted (callback);
		}

        protected void SetCallbackVideoRewardFinished(Action<bool, AdNetworks> callback) {
			foreach (KeyValuePair<AdNetworks, AdType> adTypeKeyValuePair in instance.adTypes)
				adTypeKeyValuePair.Value.SetCallbackVideoRewardFinished (callback);
		}

        protected void SetCallbackBannerStarted(Action<AdNetworks> callback) {
			foreach (KeyValuePair<AdNetworks, AdType> adTypeKeyValuePair in instance.adTypes)
				adTypeKeyValuePair.Value.SetCallbackBannerStarted (callback);
		}
    }

	public enum AdNetworks {
		AdColony,
		UnityAds,
		Vungle
	}
}
