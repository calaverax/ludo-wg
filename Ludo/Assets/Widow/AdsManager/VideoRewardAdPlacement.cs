﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Widow.Ads {
	public class VideoRewardAdPlacement {

		protected static string PLACEMENT_ID = "placementId";
		protected static string SHOW = "show";
		protected static string FREQUENCY_CAP = "frequencyCap";
		protected static string AD_NETWORK_PRIORITY = "adNetworkPriority";

		protected JSONObject json;
		protected string placementId;
		protected bool mustShow;
		protected VideoRewardFrequencyCap videoRewardFrequencyCap;
		protected List<AdNetworkPriority> adNetworkPriorities;

		public VideoRewardAdPlacement(JSONObject json) {
			this.json = json;
			this.placementId = this.json [PLACEMENT_ID].ToStringUnquoted ();
			this.mustShow = this.json [SHOW].ToBool ();
			this.videoRewardFrequencyCap = new VideoRewardFrequencyCap (this.placementId, this.json [FREQUENCY_CAP]);
			this.adNetworkPriorities = new List<AdNetworkPriority> ();
			for (int i = 0; i < this.json [AD_NETWORK_PRIORITY].Count; i++) {
				this.adNetworkPriorities.Add (new AdNetworkPriority (this.json [AD_NETWORK_PRIORITY] [i]));
			}
		}

		public JSONObject GetJson () {
			return this.json;
		}

		public string GetPlacementId () {
			return this.placementId;
		}

		public bool MustShow () {
			return this.mustShow;
		}

		public VideoRewardFrequencyCap GetVideoFrequencyCap() {
			return this.videoRewardFrequencyCap;
		}

		public List<AdNetworkPriority> GetAdNetworkPriorities () {
			return this.adNetworkPriorities;
		}

		public bool IsAvailable () {
			return this.videoRewardFrequencyCap.IsAvailable ();
		}
	}
}