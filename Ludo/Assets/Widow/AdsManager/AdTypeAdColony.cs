﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Widow.Ads;

namespace Widow.Ads
{
    public class AdTypeAdColony : AdType
    {
		#if UNITY_ANDROID
		private string AdColonyGameID = "app07f219fa227345c799";
#elif UNITY_IOS
		private string AdColonyGameID = "app07f219fa227345c799";
#endif

        AdColony.InterstitialAd InterstitialAd = null;
        AdColony.InterstitialAd RewardedAd = null;

        public override bool AdInit(List<string> placements)
        {
			isVideoRewardCallback = false;
            AdColony.Ads.OnOpened += OnVideoStarted;
            AdColony.Ads.OnClosed += OnVideoFinished;
            AdColony.Ads.OnRewardGranted += this.OnV4VCResult;
            AdColony.Ads.OnRequestInterstitial += (AdColony.InterstitialAd ad_) => {
                Debug.Log("AdColony.Ads.OnRequestInterstitial called ad_.ZoneId: " + ad_.ZoneId);
                if (ad_.ZoneId != placements[placements.Count - 2])
                    InterstitialAd = ad_;
                else
                    RewardedAd = ad_;
            };
            AdColony.Ads.OnRequestInterstitialFailed += () => {
                Debug.Log("AdColony.Ads.OnRequestInterstitialFailed called");
            };
            AdColony.Ads.OnExpiring += (AdColony.InterstitialAd ad_) => {
                Debug.Log("AdColony.Ads.OnExpiring called");

                if (ad_.ZoneId == placements[0])
                {
                    InterstitialAd = null;
                    RequestAd(placements[0]);
                }
                else
                {
                    RewardedAd = null;
                    RequestAd(placements[2]);
                }
            };
            AdColony.Ads.OnConfigurationCompleted += (List<AdColony.Zone> zones_) => {
                Debug.Log("AdColony.Ads.OnConfigurationCompleted called");
                if (zones_ == null || zones_.Count <= 0)
                {
                    Debug.Log("Configure Failed");
                }
                else
                {
                    Debug.Log("Configure Succeeded.");
                    //foreach (string placement in placements) para que no se pida más de lo que se muestra

					Debug.Log("AdColony.Ads.RequestAd.PlacementId." + placements[0]);
                    RequestAd(placements[0]);
					Debug.Log("AdColony.Ads.RequestAd.PlacementId." + placements[placements.Count - 2]);
                    RequestAd(placements[placements.Count - 2]);
                }
            };

            ConfigureAds(placements);
            
            /*
            AdColony.Ads.Configure
            (
                AdColonyGameID, // ADC App ID from adcolony.com
                "version:1.0,store:google",     // Arbitrary app version and Android app store declaration.                 
				placements.ToArray()                         // A zone ID from adcolony.com
            );*/
            return true;
        }

        public void ConfigureAds(List<string> placements)
        {
            Debug.Log("**** Configure ADC SDK ****");

            AdColony.AppOptions appOptions = new AdColony.AppOptions();
            appOptions.UserId = "Pappafoo";
            appOptions.AdOrientation = AdColony.AdOrientationType.AdColonyOrientationAll;

            AdColony.Ads.Configure(AdColonyGameID, appOptions, placements.ToArray());
        }

        public override bool ShowVideo(string placementId)
        {
			isVideoRewardCallback = false;
            Debug.Log("Show video");
			/*if (AdColony.Ads.IsVideoAvailable (placementId)) {
				AdColony.Ads.ShowVideoAd (placementId);
				return true;
			} else {
				return false;
			}*/
            if (InterstitialAd != null)
            {
                AdColony.Ads.ShowAd(InterstitialAd);
                RequestAd(placementId);
                return true;
            } else {
                return false;
            }
        }

        public void RequestAd(string placementID)
        {
            Debug.Log("**** Request Ad ****: " + placementID);

            AdColony.AdOptions adOptions = new AdColony.AdOptions();
            adOptions.ShowPrePopup = false;
            adOptions.ShowPostPopup = false;

            AdColony.Ads.RequestInterstitialAd(placementID, adOptions);
        }
        
        public override bool ShowVideoReward(string placementId)
        {
			isVideoRewardCallback = false;
			/*if (AdColony.Ads.IsV4VCAvailable(placementId))
            {
                Debug.Log("Play AdColony V4VC Ad");
				AdColony.Ads.ShowV4VC(false, placementId);
                return true;
            }
            else
            {
                Debug.Log("V4VC Ad Not Available");
                return false;
            }*/

            if (RewardedAd != null)
            {
                Debug.Log("Play AdColony V4VC Ad");
                AdColony.Ads.ShowAd(RewardedAd);
                RequestAd(placementId);
                return true;
            }
            else
            {
                Debug.Log("V4VC Ad Not Available");
                return false;
            }
        }

		public override bool ShowBanner (string placementId) {
			if (OnBannerStartedCallback != null)
				OnBannerStartedCallback (AdNetworks.AdColony);
			return false;
		}

        private void OnV4VCResult(string zoneId, bool success, string name, int amount)
        {
            if (success)
            {
                Debug.Log("V4VC SUCCESS: name = " + name + ", amount = " + amount);
                if (OnVideoFinishedCallback != null)
                    OnVideoFinishedCallback(success, AdNetworks.AdColony);
            }
            else
            {
                Debug.LogWarning("V4VC FAILED!");
            }
        }
    
		public override bool IsVideoAvailable(string placementId) {
            Debug.Log("IsVideoAvailable AdColony: " + (bool)(InterstitialAd != null) + " placementId: " + placementId);
			return InterstitialAd != null; 
		}

		public override	bool IsVideoRewardAvailable (string placementId) {
            Debug.Log("IsVideoRewardAvailable AdColony: " + (bool)(RewardedAd != null) + " placementId: " + placementId);
            return RewardedAd != null;
        }

		public override bool IsBannerAvailable (string placementId) {
			return false;
		}

    	void OnVideoStarted(AdColony.InterstitialAd ad_)
        {
			if (isVideoRewardCallback) {
				if (OnVideoRewardStartedCallback != null)
					OnVideoRewardStartedCallback (AdNetworks.AdColony);
			} else {
				if (OnVideoStartedCallback != null)
					OnVideoStartedCallback (AdNetworks.AdColony);
			}
        }

        void OnVideoFinished(AdColony.InterstitialAd ad_)
        {
			isVideoRewardCallback = false;
			if (OnVideoRewardFinishedCallback != null)
				OnVideoRewardFinishedCallback (true, AdNetworks.AdColony);
			if (OnVideoFinishedCallback != null)
				OnVideoFinishedCallback (true, AdNetworks.AdColony);
        }
    }
}