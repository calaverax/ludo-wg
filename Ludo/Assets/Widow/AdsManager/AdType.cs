﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Widow.Ads
{
    public class AdType
    {
		protected bool isVideoRewardCallback;

        protected Action<AdNetworks> OnVideoStartedCallback;
		protected Action<bool, AdNetworks> OnVideoFinishedCallback;

		protected Action<AdNetworks> OnVideoRewardStartedCallback;
		protected Action<bool, AdNetworks> OnVideoRewardFinishedCallback;

		protected Action<AdNetworks> OnBannerStartedCallback;

		public virtual bool AdInit(List<string> placements) { return false; }

		public virtual bool ShowInterstitial() { return false; }
		public virtual bool ShowVideo(string placementId) { return false; }
		public virtual bool ShowVideoReward(string placementId) { return false; }
		public virtual bool ShowNative() { return false; }
		public virtual bool ShowBanner (string placementId) { return false; }

		public virtual bool IsVideoAvailable(string placementId) { return false; }
		public virtual bool IsVideoRewardAvailable(string placementId) { return false; }
		public virtual bool IsBannerAvailable(string placementId) { return false; }

		public void SetCallbackVideoStarted(Action<AdNetworks> callback) { OnVideoStartedCallback = callback; }
		public void SetCallbackVideoFinished(Action<bool, AdNetworks> callback) { OnVideoFinishedCallback = callback; }

		public void SetCallbackVideoRewardStarted(Action<AdNetworks> callback) { OnVideoRewardStartedCallback = callback; }
		public void SetCallbackVideoRewardFinished(Action<bool, AdNetworks> callback) { OnVideoRewardFinishedCallback = callback; }

		public void SetCallbackBannerStarted(Action<AdNetworks> callback) { OnBannerStartedCallback = callback; }
    }
}
