﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Widow.Ads {
	public class AdNetworkPriority {

		protected static string AD_NETWORK = "adNetwork";
		protected static string PLACEMENT_ID = "placementId";

		protected JSONObject json;
		protected string adNetwork;
		protected string placementId;

		public AdNetworkPriority (JSONObject json) {
			this.json = json;
			this.adNetwork = this.json [AD_NETWORK].ToStringUnquoted ();
			this.placementId = this.json [PLACEMENT_ID].ToStringUnquoted ();
		}

		public JSONObject GetJson () {
			return this.json;
		}

		public string GetAdNetwork () {
			return this.adNetwork;
		}

		public string GetPlacementId () {
			return this.placementId;
		}
	}
}