﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Widow.Ads {
	public class VideoRewardFrequencyCap {

		protected static string ACTUAL_AMOUNT_PREFS_KEY = "WAM - VideoRewardAdPlacement - FrequencyCap - DailyLimit - ActualAmount - ";
		protected static string ACTUAL_DAY_PREFS_KEY = "WAM - VideoRewardAdPlacement - FrequencyCap - DailyLimit - ActualDay - ";
		protected static string LAST_TIME_SHOW_PREFS_KEY = "WAM - VideoRewardAdPlacement - FrequencyCap - TimeBetweenAds - LastTimeShow - ";
		protected static string DAILY_LIMIT = "dailyLimit";
		protected static string TIME_BETWEEN_ADS = "timeBetweenAds";

		protected JSONObject json;
		protected string placementId;
		protected int dailyLimit;
		protected int timeBetweenAds;

		public VideoRewardFrequencyCap (string placementId, JSONObject json) {
			this.json = json;
			this.placementId = placementId;
			this.dailyLimit = this.json [DAILY_LIMIT].ToInt ();
			this.timeBetweenAds = this.json [TIME_BETWEEN_ADS].ToInt ();

			if (!PlayerPrefs.HasKey (ACTUAL_AMOUNT_PREFS_KEY + this.placementId) && !PlayerPrefs.HasKey (ACTUAL_DAY_PREFS_KEY + this.placementId) && !PlayerPrefs.HasKey (LAST_TIME_SHOW_PREFS_KEY + this.placementId)) {
				PlayerPrefs.SetInt (ACTUAL_AMOUNT_PREFS_KEY + this.placementId, 0);
				PlayerPrefs.SetInt (ACTUAL_DAY_PREFS_KEY + this.placementId, (DateTime.Now.Year * 10000) + (DateTime.Now.Month * 100) + DateTime.Now.Day);
				PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, 0f);
			}
		}

		public JSONObject GetJson () {
			return this.json;
		}

		public int GetDailyLimit () {
			return this.dailyLimit;
		}

		public int GetTimeBetweenAds () {
			return this.timeBetweenAds;
		}

		public bool IsAvailable () {
			if ((PlayerPrefs.GetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId) + this.timeBetweenAds) < PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME)) {
				if (PlayerPrefs.GetInt (ACTUAL_DAY_PREFS_KEY + this.placementId) < ((DateTime.Now.Year * 10000) + (DateTime.Now.Month * 100) + DateTime.Now.Day)) {
					PlayerPrefs.SetInt (ACTUAL_AMOUNT_PREFS_KEY + this.placementId, 1);
					PlayerPrefs.SetInt (ACTUAL_DAY_PREFS_KEY + this.placementId, (DateTime.Now.Year * 10000) + (DateTime.Now.Month * 100) + DateTime.Now.Day);
					PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME));
					return true;
				} else if (PlayerPrefs.GetInt (ACTUAL_DAY_PREFS_KEY + this.placementId) == ((DateTime.Now.Year * 10000) + (DateTime.Now.Month * 100) + DateTime.Now.Day) &&
				          PlayerPrefs.GetInt (ACTUAL_AMOUNT_PREFS_KEY + this.placementId) < this.dailyLimit) {
					PlayerPrefs.SetInt (ACTUAL_AMOUNT_PREFS_KEY + this.placementId, PlayerPrefs.GetInt (ACTUAL_AMOUNT_PREFS_KEY + this.placementId) + 1);
					PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME));
					return true;
				}
			}
			return false;
		}
	}
}