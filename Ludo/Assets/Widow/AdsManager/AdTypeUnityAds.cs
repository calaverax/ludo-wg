﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ADS
using UnityEngine.Advertisements;
#endif

namespace Widow.Ads {
	public class AdTypeUnityAds : AdType {

#if UNITY_ANDROID
		protected string gameId = "1696461";
#elif UNITY_IOS
		protected string gameId = "1696462";
#endif

        public override bool AdInit (List<string> placements) {
			isVideoRewardCallback = false;
            #if UNITY_ADS
            if (Advertisement.isSupported)
#if STAGE
                Advertisement.Initialize(gameId, true);
#else
                Advertisement.Initialize(gameId, false);
#endif
#endif

            return true;
		}

		public override bool ShowInterstitial() {
			return false;
		}

		public override bool ShowVideo (string placementId) {
			isVideoRewardCallback = false;
			Debug.Log ("Show video");
#if UNITY_ADS
            if (Advertisement.IsReady (placementId)) {
				Advertisement.Show (placementId, GetShowOptions ());
				if (Advertisement.isShowing)
					OnAdStarted ();
				return true;
			} else 
#endif
            {
				return false;
			}
		}

		public override bool IsVideoAvailable (string placementId) {
#if UNITY_ADS
			Debug.LogError ("Unity Ads - Video  Available - " + Advertisement.IsReady (placementId).ToString () + " - PlacementId - " + placementId);
            return Advertisement.IsReady (placementId);
#else
            return false;
#endif
        }
        public override bool ShowVideoReward (string placementId) {
#if UNITY_ADS
			isVideoRewardCallback = true;
			Debug.Log ("Show video reward.");
			if (Advertisement.IsReady (placementId)) {
				Advertisement.Show (placementId, GetShowOptions ());
				if (Advertisement.isShowing)
					OnAdStarted ();
				return true;
			} else
#endif
            {
				return false;
			}
		}

		public override bool IsVideoRewardAvailable (string placementId) {
#if UNITY_ADS
			Debug.LogError ("Unity Ads - Video Reward Available - " + Advertisement.IsReady (placementId).ToString () + " - PlacementId - " + placementId);
			return Advertisement.IsReady (placementId);
#else
            return false;
#endif

        }

		public override bool ShowBanner (string placementId) {
			if (OnBannerStartedCallback != null)
				OnBannerStartedCallback (AdNetworks.UnityAds);
			return false;
		}

		public override bool IsBannerAvailable (string placementId) {
			return false;
		}

#if UNITY_ADS
		private ShowOptions GetShowOptions () {
			ShowOptions options = new ShowOptions ();
			options.resultCallback += OnAdFinished;
			return options;
		}

		void OnAdStarted () {
			if (isVideoRewardCallback) {
				isVideoRewardCallback = false;
				if (OnVideoRewardStartedCallback != null)
					OnVideoRewardStartedCallback (AdNetworks.UnityAds);
			} else {
				if (OnVideoStartedCallback != null)
					OnVideoStartedCallback (AdNetworks.UnityAds);
			}
		}

		void OnAdFinished (ShowResult result) {
			bool finished = false;
			switch (result) {
			case ShowResult.Finished:
				finished = true;
				break;
			default:
				finished = false;
				break;
			}


			if (OnVideoRewardFinishedCallback != null) {
				OnVideoRewardFinishedCallback (finished, AdNetworks.UnityAds);
				OnVideoRewardFinishedCallback = null;
			}
			if (OnVideoFinishedCallback != null) {
				OnVideoFinishedCallback (finished, AdNetworks.UnityAds);
				OnVideoFinishedCallback = null;
			}
		}
#endif
	}
}