﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Widow.Ads
{
    public class AdsTesting : MonoBehaviour
    {
        void Awake()
        {
#if !STAGE
            Destroy(this.gameObject);
            return;
#else
            // Test_AdColony_BTN
            transform.GetChild(0).GetChild(0).GetComponent<Button>().onClick.AddListener(TestAdsAdColony);
            // Test_UnityAds_BTN
            transform.GetChild(0).GetChild(1).GetComponent<Button>().onClick.AddListener(TestAdsUnity);
#endif
        }

        public void TestAdsAdColony()
        {
            Debug.Log("Sending ShowVideo to AdColony");
            Ads.instance.ShowVideo("AdColony", "InterstitialVideo", null, (bool adFinished, AdNetworks adnetwork) =>
            {
                Dumb();
            });
        }
        public void TestAdsUnity()
        {
            Debug.Log("Sending ShowVideo to UnityAds");
            Ads.instance.ShowVideo("UnityAds", "InterstitialVideo", null, (bool adFinished, AdNetworks adnetwork) =>
            {
                Dumb();
            });
        }

        private void Dumb()
        {

        }
    }
}
