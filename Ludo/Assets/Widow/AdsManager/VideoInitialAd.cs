﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Widow.Ads {
	public class VideoInitialAd {

		protected static string LAST_TIME_SHOW_PREFS_KEY = "WAM - VideoAdPlacement - InitialAd - LastTimeShow - ";
		protected static string ACTUAL_REQUESTS_PREFS_KEY = "WAM - VideoAdPlacement - InitialAd - ActualRequests - ";
		protected static string ACTUAL_SESSIONS_PREFS_KEY = "WAM - VideoAdPlacement - InitialAd - ActualSessions - ";
		protected static string SHOW_INITIAL_AD = "WAM - VideoAdPlacement - InitialAd - ShowInitialAd - ";
		protected static string MODE = "mode";
		protected static string SECONDS = "seconds";
		protected static string REQUESTS = "requests";
		protected static string SESSIONS = "sessions";

		protected string placementId;
		protected JSONObject json;
		protected VideoInitialAdMode initialAdMode;
		protected int seconds;
		protected int requests;
		protected int sessions;

		public VideoInitialAd (string placementId, JSONObject json) {
			this.placementId = placementId;
			this.json = json;
			this.initialAdMode = (VideoInitialAdMode)this.json [MODE].ToInt ();
			this.seconds = this.json [SECONDS].ToInt ();
			this.requests = this.json [REQUESTS].ToInt ();
			this.sessions = this.json [SESSIONS].ToInt ();

			if (!PlayerPrefs.HasKey (LAST_TIME_SHOW_PREFS_KEY + this.placementId) && !PlayerPrefs.HasKey (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) && !PlayerPrefs.HasKey (SHOW_INITIAL_AD + this.placementId)) {
				PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME));
				PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, 0);
				PlayerPrefs.SetInt (SHOW_INITIAL_AD + this.placementId, 1);
			}
			PlayerPrefs.SetInt (ACTUAL_SESSIONS_PREFS_KEY + this.placementId, PlayerPrefs.HasKey (ACTUAL_SESSIONS_PREFS_KEY + this.placementId) ? PlayerPrefs.GetInt (ACTUAL_SESSIONS_PREFS_KEY + this.placementId) + 1 : 0);
		}

		public JSONObject GetJson () {
			return this.json;
		}

		public VideoInitialAdMode GetInitialAdMode () {
			return this.initialAdMode;
		}

		public int GetSeconds () {
			return this.seconds;
		}

		public int GetRequests () {
			return this.requests;
		}

		public int GetSessions () {
			return this.sessions;
		}

		public bool IsFirstAd () {
			return PlayerPrefs.GetInt (SHOW_INITIAL_AD + this.placementId) == 1;
		}

		public bool IsAvailable () {
			PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) + 1);
			bool isAvailable = false;
			switch ((int) this.initialAdMode) {
			case 0:
				isAvailable = PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME) >= (PlayerPrefs.GetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId) + this.GetSeconds());
				break;
			case 1:
				isAvailable = PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) >= this.GetRequests ();
				break;
			case 2:
				isAvailable = PlayerPrefs.GetInt (ACTUAL_SESSIONS_PREFS_KEY + this.placementId) >= this.GetSessions ();
				break;
			case 3:
				isAvailable = PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME) >= (PlayerPrefs.GetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId) + this.GetSeconds()) && PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) >= this.GetRequests ();
				break;
			case 4:
				isAvailable = PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME) >= (PlayerPrefs.GetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId) + this.GetSeconds()) || PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) >= this.GetRequests ();
				break;
			case 5:
				isAvailable = PlayerPrefs.GetInt (ACTUAL_SESSIONS_PREFS_KEY + this.placementId) >= this.GetSessions () && PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) >= this.GetRequests ();
				break;
			case 6:
				isAvailable = PlayerPrefs.GetInt (ACTUAL_SESSIONS_PREFS_KEY + this.placementId) >= this.GetSessions () || PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) >= this.GetRequests ();
				break;
			default:
				break;
			}
			if (isAvailable)
				PlayerPrefs.SetInt (SHOW_INITIAL_AD + this.placementId, 0);
			return isAvailable;
		}
	}

	public enum VideoInitialAdMode {
		SecondsOnly = 0,
		RequestOnly = 1,
		SessionsOnly = 2,
		SecondsAndRequests = 3,
		SecondsOrRequests = 4,
		SessionsAndRequests = 5,
		SessionsOrRequests = 6
	}
}