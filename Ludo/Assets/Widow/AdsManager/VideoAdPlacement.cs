﻿using System.Collections;
using System.Collections.Generic;

namespace Widow.Ads {
	public class VideoAdPlacement {

		protected static string PLACEMENT_ID = "placementId";
		protected static string SHOW = "show";
		protected static string INITIAL_AD = "initialAd";
		protected static string FREQUENCY_CAP = "frequencyCap";
		protected static string AD_NETWORK_PRIORITY = "adNetworkPriority";

		protected JSONObject json;
		protected string placementId;
		protected bool mustShow;
		protected VideoFrequencyCap videoFrequencyCap;
		protected VideoInitialAd videoInitialAd;
		protected List<AdNetworkPriority> adNetworkPriorities;

		public VideoAdPlacement (JSONObject json) {
			this.json = json;
			this.placementId = this.json [PLACEMENT_ID].ToStringUnquoted ();
			this.mustShow = this.json [SHOW].ToBool ();
			this.videoInitialAd = new VideoInitialAd (this.placementId, this.json [INITIAL_AD]);
			if (!this.videoInitialAd.IsFirstAd())
				this.videoFrequencyCap = new VideoFrequencyCap (this.placementId, this.json [FREQUENCY_CAP]);
			this.adNetworkPriorities = new List<AdNetworkPriority> ();
			for (int i = 0; i < this.json [AD_NETWORK_PRIORITY].Count; i++) {
				this.adNetworkPriorities.Add (new AdNetworkPriority (this.json [AD_NETWORK_PRIORITY] [i]));
			}
		}

		public JSONObject GetJson () {
			return this.json;
		}

		public string GetPlacementId () {
			return this.placementId;
		}

		public bool MustShow () {
			return this.mustShow;
		}

		public VideoFrequencyCap GetVideoFrequencyCap() {
			return this.videoFrequencyCap;
		}

		public VideoInitialAd GetVideoInitialAd () {
			return this.videoInitialAd;
		}

		public List<AdNetworkPriority> GetAdNetworkPriorities () {
			return this.adNetworkPriorities;
		}

		public bool IsAvailable () {
			if (this.videoInitialAd.IsFirstAd ()) {
				bool available = this.videoInitialAd.IsAvailable ();
				if (available)
					this.videoFrequencyCap = new VideoFrequencyCap (this.placementId, this.json [FREQUENCY_CAP]);
				return available;
			}
			return this.videoFrequencyCap.IsAvailable ();
		}
	}
}