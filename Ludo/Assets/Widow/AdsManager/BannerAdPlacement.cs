﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Widow.Ads {
	public class BannerAdPlacement {

		protected static string PLACEMENT_ID = "placementId";
		protected static string SHOW = "show";
		protected static string AD_NETWORK_PRIORITY = "adNetworkPriority";
		
		protected JSONObject json;
		protected string placementId;
		protected bool mustShow;
		protected List<AdNetworkPriority> adNetworkPriorities;

		public BannerAdPlacement (JSONObject json) {
            adNetworkPriorities = new List<AdNetworkPriority>();
            this.json = json;
			this.placementId = this.json [PLACEMENT_ID].ToStringUnquoted ();
			this.mustShow = this.json [SHOW].ToBool ();
			for (int i = 0; i < this.json [AD_NETWORK_PRIORITY].Count; i++) {
				this.adNetworkPriorities.Add (new AdNetworkPriority (this.json [AD_NETWORK_PRIORITY] [i]));
			}
		}

		public JSONObject GetJson () {
			return this.json;
		}

		public string GetPlacementId () {
			return this.placementId;
		}

		public bool MustShow () {
			return this.mustShow;
		}

		public List<AdNetworkPriority> GetAdNetworkPriorities () {
			return this.adNetworkPriorities;
		}
	}
}