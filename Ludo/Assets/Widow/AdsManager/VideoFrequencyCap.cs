﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Widow.Ads {
	public class VideoFrequencyCap {

		protected static string LAST_TIME_SHOW_PREFS_KEY = "WAM - VideoAdPlacement - FrequencyCap - LastTimeShow - ";
		protected static string ACTUAL_REQUESTS_PREFS_KEY = "WAM - VideoAdPlacement - FrequencyCap - ActualRequests - ";
		protected static string SESSION_RESET = "sessionReset";
		protected static string MODE = "mode";
		protected static string SECONDS = "seconds";
		protected static string REQUESTS = "requests";

		protected JSONObject json;
		protected string placementId;
		protected bool sessionReset;
		protected VideoFrequencyCapMode frequencyCapMode;
		protected int seconds;
		protected int requests;

		protected float lastTimeShow;
		protected int actualRequests;

		public VideoFrequencyCap (string placementId, JSONObject json) {
			this.placementId = placementId;
			this.json = json;
			this.sessionReset = this.json [SESSION_RESET].ToBool ();
			this.frequencyCapMode = (VideoFrequencyCapMode)this.json [MODE].ToInt ();
			this.seconds = this.json [SECONDS].ToInt ();
			this.requests = this.json [REQUESTS].ToInt ();

			if (this.sessionReset) {
				this.lastTimeShow = PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME + this.placementId);
				this.actualRequests = 0;
			} else if (!PlayerPrefs.HasKey (LAST_TIME_SHOW_PREFS_KEY + this.placementId) && !PlayerPrefs.HasKey (ACTUAL_REQUESTS_PREFS_KEY + this.placementId)) {
				PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME + this.placementId));
				PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, 0);
			}
		}

		public JSONObject GetJson () {
			return this.json;
		}

		public bool GetSessionReset () {
			return this.sessionReset;
		}

		public VideoFrequencyCapMode GetMode () {
			return this.frequencyCapMode;
		}

		public int GetSeconds () {
			return this.seconds;
		}

		public int GetRequests () {
			return this.requests;
		}

		public bool IsAvailable () {
			switch ((int) this.frequencyCapMode) {
			case 0:
				if (PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME) >= ((this.sessionReset ? this.lastTimeShow : PlayerPrefs.GetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId)) + this.GetSeconds())) {
					if (this.sessionReset) {
						this.lastTimeShow = PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME + this.placementId);
					} else {
						PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME + this.placementId));
					}
					return true;
				}
				return false;
			case 1:
				if (this.sessionReset) {
					this.actualRequests++;
				} else {
					PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) + 1);
				}
				if (this.GetRequests() <= (this.sessionReset ? this.actualRequests : PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId))) {
					if (this.sessionReset) {
						this.actualRequests = 0;
					} else {
						PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, 0);
					}
					return true;
				}
				return false;
			case 2:
				if (this.sessionReset) {
					this.actualRequests++;
				} else {
					PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) + 1);
				}
				if ((PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME) >= ((this.sessionReset ? this.lastTimeShow : PlayerPrefs.GetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId)) + this.GetSeconds())) && 
					(this.GetRequests() <= (this.sessionReset ? this.actualRequests : PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId)))) {
					if (this.sessionReset) {
						this.lastTimeShow = PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME);
						this.actualRequests = 0;
					} else {
						PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, 0);
						PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME));
					}
					return true;
				}
				return false;
			case 3:
				if (this.sessionReset) {
					this.actualRequests++;
				} else {
					PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId) + 1);
				}
				if ((PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME) >= ((this.sessionReset ? this.lastTimeShow : PlayerPrefs.GetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId)) + this.GetSeconds())) || 
					(this.GetRequests() <= (this.sessionReset ? this.actualRequests : PlayerPrefs.GetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId)))) {
					if (this.sessionReset) {
						this.lastTimeShow = PlayerPrefs.GetFloat (Ads.TOTAL_PLAYED_TIME);
						this.actualRequests = 0;
					} else {
						PlayerPrefs.SetFloat (LAST_TIME_SHOW_PREFS_KEY + this.placementId, PlayerPrefs.GetFloat(Ads.TOTAL_PLAYED_TIME));
						PlayerPrefs.SetInt (ACTUAL_REQUESTS_PREFS_KEY + this.placementId, 0);
					}
					return true;
				}
				return false;
			default:
				return false;
			}
		}
	}

	public enum VideoFrequencyCapMode {
		SecondsOnly = 0,
		RequestsOnly = 1,
		SecondsAndRequests = 2,
		SecondsOrRequests = 3
	}
}