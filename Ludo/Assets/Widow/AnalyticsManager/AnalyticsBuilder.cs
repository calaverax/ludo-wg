﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Widow.Analytics
{
    public enum DataType
    {
        //facebook
        RegisteringTo,
        TutorialName,
        LevelName,
        DurationSeconds,
        ItemName,
        ItemID,
        Currency,
        ItemAmount,
        ItemUsedAmount,
        CurrencyChangeAmount,
        Date,
        SpentReason,
        AdType, //video or banner
        AdConclusion, //viewed, cancelled, etc
        
        //JDS
        Game,
        PaymentMethod,

        //game Analytics

        //google Analytics
        Category,
        Action,
        Label,
        Value,
        TransactionId,
        Affiliation,
        ItemCategory,
        Screen,
    }

    public struct RegisteringTypes
    {
        public const string Facebook = "Facebook";
        public const string Mail = "Email";
        public const string JDS = "JDS";
        public const string GooglePlus = "Google";
    }

    public class AnalyticsBuilder
    {
        private Dictionary<DataType, object> data;

        public AnalyticsBuilder()
        {
            data = new Dictionary<DataType, object>();
        }

        public AnalyticsBuilder Add(DataType dataType, object val)
        {
            data.Add(dataType, val);
            return this;
        }

        public bool assert(DataType[] typesToAssert)
        {
            foreach (DataType dt in typesToAssert)
            {
                if (!data.ContainsKey(dt))
                {
                    Debug.LogError("Analytics Assertion failed for " + dt);
                    return false;
                }
            }
            return true;
        }

        public object GetVal(DataType dataType)
        {
            if (data.ContainsKey(dataType))
                return data[dataType];
            return null;
        }

        public object this[DataType i]
        {
            get { return data[i]; }
            //set { data[i] = value; }
        }
    }

}
