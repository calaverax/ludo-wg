﻿#if false
using UnityEngine;
using System.Collections;
using System;

namespace Widow.Analytics
{
    public class AnalyticsTypeGoogleAnalytics : AnalyticsType
    {
        private GoogleAnalyticsV3 googleAnalytics;
        private string userId = "";

        public AnalyticsTypeGoogleAnalytics(GoogleAnalyticsV3 ga)
        {
            googleAnalytics = ga;
        }

        public override void LevelStarted(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.Category, DataType.Action, DataType.Label, DataType.Value }))
                return;

            googleAnalytics.LogEvent(new EventHitBuilder()
                                         .SetEventCategory(ab[DataType.Category].ToString())
                                         .SetEventAction(ab[DataType.Action].ToString())
                                         .SetEventLabel(ab[DataType.Label].ToString())
                                         .SetEventValue((long)((int)ab[DataType.Value]))
                                         .SetCustomDimension(1, userId)
                                        //.SetCustomDimension (2, UserData.Instance.paymentMethod)
                                        /*.SetCustomDimension (3, loginType)*/);
        }

        public override void RegistrationCompleted(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.RegisteringTo, DataType.Category, DataType.Action, DataType.Label }))
                return;

            googleAnalytics.LogEvent(new EventHitBuilder()
                             .SetEventCategory(ab[DataType.Category].ToString())
                             .SetEventAction(ab[DataType.Action].ToString())
                             .SetEventLabel(ab[DataType.Label].ToString())
                             .SetCustomDimension(1, ab[DataType.RegisteringTo].ToString())
                            //.SetCustomDimension (2, UserData.Instance.paymentMethod)
                            /*.SetCustomDimension (3, loginType)*/);
        }

        public override void PurchaseSuccess(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.Category, DataType.Action, DataType.Label, DataType.TransactionId, DataType.Affiliation, DataType.ItemCategory, DataType.ItemName, DataType.ItemID, DataType.Currency, DataType.ItemAmount }))
                return;

            float itemAmount = 0;
            Single.TryParse(ab[DataType.ItemAmount].ToString(), out itemAmount);

            googleAnalytics.LogEvent(new EventHitBuilder()
                             .SetEventCategory(ab[DataType.Category].ToString())
                             .SetEventAction(ab[DataType.Action].ToString())
                             .SetEventLabel(ab[DataType.Label].ToString())
                             .SetCustomDimension(1, userId)
                            //.SetCustomDimension (2, UserData.Instance.paymentMethod)
                            /*.SetCustomDimension (3, loginType)*/);

            // Builder Hit with all Transaction parameters.
            googleAnalytics.LogTransaction(new TransactionHitBuilder()
                            .SetTransactionID(ab[DataType.TransactionId].ToString())
                            .SetAffiliation(ab[DataType.Affiliation].ToString())
                            .SetRevenue(itemAmount)
                            .SetTax(0)
                            .SetShipping(0.0)
                            .SetCurrencyCode(ab[DataType.Currency].ToString())
                            .SetCustomDimension(1, userId));

            // Builder Hit with all Item parameters.
            googleAnalytics.LogItem(new ItemHitBuilder()
                            .SetTransactionID(ab[DataType.TransactionId].ToString())
                            .SetName(ab[DataType.ItemName].ToString())
                            .SetSKU(ab[DataType.ItemID].ToString())
                            .SetCategory(ab[DataType.ItemCategory].ToString())
                            .SetPrice(itemAmount)
                            .SetQuantity(1)
                            .SetCurrencyCode(ab[DataType.Currency].ToString())
                            .SetCustomDimension(1, userId));
        }

        public override void LogScreen(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.Screen }))
                return;

            //Builder Hit with all App View parameters (all parameters required):
            googleAnalytics.LogScreen(new AppViewHitBuilder()
                .SetScreenName(ab[DataType.Screen].ToString())
                .SetCustomDimension(1, userId));
        }

        public override void CustomEvent(string eventName, int count, string[] data = null)
        {
            if (data == null || data.Length < 2)
                return;

            string label = "";
#if !UNITY_EDITOR
            if (data.Length > 2)
                label = data[2];

            googleAnalytics.LogEvent (new EventHitBuilder ()
             		                    .SetEventCategory (data[0])
             		                    .SetEventAction (data[1])
             		                    .SetEventLabel (data[2])
             		                    .SetEventValue ((long)count)
             		                    .SetCustomDimension (1, userId)
             	                        //.SetCustomDimension (2, UserData.Instance.paymentMethod)
             						    /*.SetCustomDimension (3, loginType)*/);
#endif
        }

        public override void SetUserId(string UserId)
        {
            userId = UserId;
        }
    }
}
#endif