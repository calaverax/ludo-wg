﻿#if true

using System;
using System.Collections.Generic;
using Facebook;
using UnityEngine;
using Facebook.Unity;

namespace Widow.Analytics
{
    /// <summary>
    /// For facebook sdk version 7.4.0
    /// </summary>
    public class AnalyticsTypeFacebook7_4 : AnalyticsType
    {
        public Dictionary<string, DateTime> levelsDurations;
        private DateTime sessionStart;
        private string userId = "";

        struct ParticularEvents
        {
            public const string RegistrationFailedEvent = "Registration Failed";
            public const string LevelStartedEvent = "Level Started";
            public const string LevelRestartedEvent = "Level Restarted";
            public const string LevelExitedEvent = "Level Exited";
            public const string LevelLostEvent = "Level Lost";
            public const string LevelDurationEvent = "Level Duration";
            public const string PurchaseFailedEvent = "Purchase Failed";
            public const string SessionStartEvent = "Session Start";
            public const string SessionDurationEvent = "Session Duration";
            public const string SessionEndEvent = "Session End";
            public const string GainCredits = "Gain Credits";
            public const string Ad = "Ad";
        }

        public override void AppLaunch(AnalyticsBuilder ab)
        {
            var appStartParameters = new Dictionary<string, object>();

            appStartParameters["user"] = userId;
            appStartParameters["device_id"] = SystemInfo.deviceUniqueIdentifier;
            appStartParameters["bundle_id"] = Application.identifier.ToString();
            appStartParameters["platform"] = Application.platform.ToString();
            appStartParameters["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            appStartParameters["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            //appStartParameters["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //appStartParameters["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();

            FB.LogAppEvent(AppEventName.ActivatedApp, null, appStartParameters);

        }

        public override void RegistrationCompleted(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.RegisteringTo }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.RegistrationMethod] = ab[DataType.RegisteringTo];

            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();

            p = AssignUserId(p);
            FB.LogAppEvent(AppEventName.CompletedRegistration, 0, p);
        }

        public override void RegistrationFail(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.RegisteringTo }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.RegistrationMethod] = ab[DataType.RegisteringTo];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.RegistrationFailedEvent, 0, p);
        }

        public override void LevelStarted(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.LevelName }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Level] = ab[DataType.LevelName];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);

            if (levelsDurations == null)
                levelsDurations = new Dictionary<string, DateTime>();

            if (levelsDurations.ContainsKey(ab[DataType.LevelName].ToString()))
            {
                levelsDurations[ab[DataType.LevelName].ToString()] = DateTime.Now;
            }
            else
            {
                levelsDurations.Add(ab[DataType.LevelName].ToString(), DateTime.Now);
            }

            FB.LogAppEvent(ParticularEvents.LevelStartedEvent, 0, p);
        }

        public override void LevelRestarted(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.LevelName }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Level] = ab[DataType.LevelName];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.LevelRestartedEvent, 0, p);

            LogLevelDuration(ab[DataType.LevelName].ToString());
        }

        public override void LevelExited(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.LevelName }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Level] = ab[DataType.LevelName];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.LevelExitedEvent, 0, p);

            LogLevelDuration(ab[DataType.LevelName].ToString());
        }

        public override void LevelCompleted(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.LevelName }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Level] = ab[DataType.LevelName];
            p[AppEventParameterName.Success] = true;
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(AppEventName.AchievedLevel, 0, p);

            LogLevelDuration(ab[DataType.LevelName].ToString());
        }

        public override void LevelLost(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.LevelName }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Level] = ab[DataType.LevelName];
            p[AppEventParameterName.Success] = false;
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.LevelLostEvent, 0, p);

            LogLevelDuration(ab[DataType.LevelName].ToString());
        }

        public override void LevelDuration(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.LevelName, DataType.DurationSeconds }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Level] = ab[DataType.LevelName];
            p[AppEventParameterName.Description] = ab[DataType.DurationSeconds];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.LevelDurationEvent, 0, p);
        }

        private void LogLevelDuration(string levelName)
        {
            if (!levelsDurations.ContainsKey(levelName))
                return;
            DateTime start = levelsDurations[levelName];

            TimeSpan duration = DateTime.Now - start;
            levelsDurations.Remove(levelName);

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Level] = levelName;
            p[AppEventParameterName.Description] = duration.ToString();
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.LevelDurationEvent, 0, p);
        }

        public override void PurchaseStart(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.ItemName, DataType.ItemID, DataType.Currency, DataType.ItemAmount }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.ContentType] = ab[DataType.ItemName];
            p[AppEventParameterName.ContentID] = ab[DataType.ItemID];
            p[AppEventParameterName.Currency] = ab[DataType.Currency];
            p[AppEventParameterName.NumItems] = ab[DataType.ItemAmount];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(AppEventName.InitiatedCheckout, 0, p);
        }

        public override void PurchaseSuccess(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.ItemName, DataType.ItemID, DataType.Currency, DataType.ItemAmount }))
                return;

            float itemAmount = 0;
            Single.TryParse(ab[DataType.ItemAmount].ToString(), out itemAmount);
            //itemAmount *= (float)0.7;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.ContentType] = ab[DataType.ItemName];
            p[AppEventParameterName.ContentID] = ab[DataType.ItemID];
            p[AppEventParameterName.Currency] = ab[DataType.Currency];
            p[AppEventParameterName.NumItems] = itemAmount;
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogPurchase(itemAmount, ab[DataType.Currency].ToString(), p);

            FB.LogAppEvent("Purchases Extra Data", 0, p);
        }

        public override void PurchaseFail(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.ItemName, DataType.ItemID, DataType.Currency, DataType.ItemAmount }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.ContentType] = ab[DataType.ItemName];
            p[AppEventParameterName.ContentID] = ab[DataType.ItemID];
            p[AppEventParameterName.Currency] = ab[DataType.Currency];
            p[AppEventParameterName.NumItems] = ab[DataType.ItemAmount];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.PurchaseFailedEvent, 0, p);
        }

        public override void SessionStart(AnalyticsBuilder ab)
        {
            sessionStart = DateTime.Now;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Description] = sessionStart.ToString();
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);

            if (FB.IsInitialized)
                FB.LogAppEvent(ParticularEvents.SessionStartEvent, 0, p);
        }

        public override void SessionDuration(AnalyticsBuilder ab)
        {
            TimeSpan duration = DateTime.Now - sessionStart;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.NumItems] = duration.ToString();
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.SessionDurationEvent, 0, p);
        }

        public override void SessionEnd(AnalyticsBuilder ab)
        {
            var p = new Dictionary<string, object>();
            p[AppEventParameterName.ContentType] = userId;
            p[AppEventParameterName.Description] = DateTime.Now.ToString();
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            FB.LogAppEvent(ParticularEvents.SessionEndEvent, 0, p);
            //FB.AppEvents.LogEvent(ParticularEvents.SessionEndEvent, 0, p);
        }

        public override void CurrencyChanged(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.CurrencyChangeAmount }))
                return;

            float amount = 0;
            Single.TryParse(ab[DataType.CurrencyChangeAmount].ToString(), out amount);

            if (amount == 0)
            {
                Debug.LogWarning("Logging CurrencyChanged as 0, D'oh!");
                return;
            }

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.NumItems] = amount;
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            if (ab.GetVal(DataType.SpentReason) != null)
                p[DataType.SpentReason.ToString()] = ab[DataType.SpentReason];

            if (ab.GetVal(DataType.ItemID) != null)
                p[AppEventParameterName.ContentID] = ab[DataType.ItemID];


            if (amount > 0) //(+) change
            {
                FB.LogAppEvent(ParticularEvents.GainCredits, 0, p);
            }
            else //(-) change
            {
                FB.LogAppEvent(AppEventName.SpentCredits, amount, p);
            }
        }

        public override void AdEvent(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.AdType, DataType.AdConclusion }))
                return;

            var p = new Dictionary<string, object>();
            p[AppEventParameterName.Description] = ab[DataType.AdConclusion];
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);
            FB.LogAppEvent(ParticularEvents.Ad + " " + ab[DataType.AdType], 0, p);
        }

        public override void CustomEvent(string eventName, int count, string[] data = null)
        {
            var p = new Dictionary<string, object>();
            if (data != null)
            {
                foreach (string s in data)
                    p[s] = s;
            }
            p["user"] = userId;
            p["device_id"] = SystemInfo.deviceUniqueIdentifier;
            p["bundle_id"] = Application.identifier.ToString();
            p["platform"] = Application.platform.ToString();
            p["bundle_code"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //p["bundle_code"] = VersionTrackerFromFile.Instance.GetBundleVersion();
            //            p["bundle_version"] = VersionTrackerFromFile.Instance.GetBuildNumber();
            p = AssignUserId(p);

            FB.LogAppEvent(eventName, count, p);
        }

        public override void SetUserId(string UserId)
        {
            userId = UserId;
        }

        private Dictionary<string, object> AssignUserId(Dictionary<string, object> p)
        {
            if (userId == "")
                p["UserId"] = SystemInfo.deviceUniqueIdentifier;
            else
                p["UserId"] = userId;

            return p;
        }
    }
}
#endif

