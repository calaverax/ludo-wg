//#define USE_GOOGLE_ANALYTICS
#define USE_GAME_ANALYTICS
#define USE_FACEBOOK_ANALYTICS
//#define USE_FACEBOOK_ANALYTICS_6_2_2
#define USE_JDS_CRM

using UnityEngine;
using System.Collections.Generic;
#if USE_FACEBOOK_ANALYTICS
using Facebook.Unity;
#endif

namespace Widow.Analytics
{
    public class Analytics 
    {
        public delegate void OnAnalyticsInstanceCreatedAction();
        public static event OnAnalyticsInstanceCreatedAction onAnalyticsInstanceCreated;

        private List<AnalyticsType> AnaliticsTypes;

        private static Analytics instance;

        public static void Init()
        {
            Get();
        }

        public static Analytics Get()
        {
            return instance ?? (instance = new Analytics());
        }

        private Analytics()
        {
            AnaliticsTypes = new List<AnalyticsType>();
#if USE_FACEBOOK_ANALYTICS_6_2_2
                if (FB.IsInitialized) {
                    FB.ActivateApp();
                } else {
                    //Handle FB.Init
                    FB.Init(() => {
                        FB.ActivateApp();
                    });
                }
                AnalyticsType facebook = new AnalyticsTypeFacebookDeprecated();
                AnaliticsTypes.Add(facebook);
#endif
#if USE_FACEBOOK_ANALYTICS
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else {
                //Handle FB.Init
                FB.Init(() => {
                    FB.ActivateApp();
                });
            }
            AnalyticsType facebook = new AnalyticsTypeFacebook7_4();
            AnaliticsTypes.Add(facebook);
#endif
#if USE_JDS_CRM
            AnalyticsType JDS_CRM = new AnalyticsTypeJDS();
                AnaliticsTypes.Add(JDS_CRM);
#endif
#if USE_GAME_ANALYTICS
                AnalyticsType gameAnalytics = new AnalyticsTypeGameAnalytics();
                AnaliticsTypes.Add(gameAnalytics);
#endif
#if USE_GOOGLE_ANALYTICS
                AnalyticsType googleAnalytics = new AnalyticsTypeGoogleAnalytics(ga);
                AnaliticsTypes.Add(googleAnalytics);
#endif

            OnAnalyticsInstanceCreatedInternalEvent();
        }

#region instance events
        private void OnAnalyticsInstanceCreatedInternalEvent()
        {
            if (onAnalyticsInstanceCreated != null)
                onAnalyticsInstanceCreated();
        }
#endregion

#region identification methods
        public void SendIdentificationEvent()
        {

        }
#endregion

        /// <summary>
        /// TODO
        /// </summary>
        public void ExampleEvent()
        {
            Debug.LogError("Analytics - Iniciando el envio de example event.");
            //GameAnalytics.SetFacebookId("IdEjemplo:PaisEjemplo");
            Debug.LogError("Analytics - Seteo el customer ID in FacebookId.");
            //GameAnalytics.SetBirthYear (88);
            Debug.LogError("Analytics - seteo el birth year.");
            //GameAnalytics.SetGender (GA_Setup.GAGender.Male);
            Debug.LogError("Analytics - seteo el gender.");
            //GameAnalytics.NewDesignEvent("Testing");
            Debug.LogError("Analytics - envio el design event.");

        }

        public static void AppInstall(AnalyticsBuilder ab = null) { foreach (var AS in Get().AnaliticsTypes) AS.AppInstall(ab); }
        public static void AppLaunch(AnalyticsBuilder ab = null) { foreach (var AS in Get().AnaliticsTypes) AS.AppLaunch(ab); }

        public static void RegistrationCompleted(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.RegistrationCompleted(ab); }
        public static void RegistrationFail(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.RegistrationFail(ab); }

        public static void TutorialCompleted(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.TutorialCompleted(ab); }
        public static void LevelStarted(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.LevelStarted(ab); }
        public static void LevelRestarted(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.LevelRestarted(ab); }
        public static void LevelExited(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.LevelExited(ab); }
        public static void LevelCompleted(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.LevelCompleted(ab); }
        public static void LevelLost(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.LevelLost(ab); }
        public static void LevelDuration(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.LevelDuration(ab); }

        public static void PurchaseStart(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.PurchaseStart(ab); }
        public static void PurchaseSuccess(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.PurchaseSuccess(ab); }
        public static void PurchaseFail(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.PurchaseFail(ab); }

        public static void SocialInvite(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.SocialInvite(ab); }
        public static void SocialRequest(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.SocialRequest(ab); }

        public static void SessionDuration(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.SessionDuration(ab); }
        public static void SessionStart(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.SessionStart(ab); }
        public static void SessionEnd(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.SessionEnd(ab); }

        public static void Error(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.Error(ab); }

        public static void ButtonClicked(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.ButtonClicked(ab); }

        public static void ItemUsed(AnalyticsBuilder ab)  { foreach (var AS in Get().AnaliticsTypes) AS.ItemUsed(ab); }

        public static void CurrencyChanged(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.CurrencyChanged(ab); }

        public static void AdEvent(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.AdEvent(ab); }

        public static void LogScreen(AnalyticsBuilder ab) { foreach (var AS in Get().AnaliticsTypes) AS.LogScreen(ab); }

        public static void CustomEvent(string eventName, int count, string[]data = null) { foreach (var AS in Get().AnaliticsTypes) AS.CustomEvent(eventName, count, data); }

        public static void SetUserId(string userId) { foreach (var AS in Get().AnaliticsTypes) AS.SetUserId(userId); }

    }

}