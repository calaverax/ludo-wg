﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Widow.DynamicValuesPlugin;

namespace Widow.Analytics
{
    public class AnalyticsTypeJDS : AnalyticsType
    {
#if STAGE
        private const string BASE_URL = "https://www.jdsservicesstage.com/crm/";
#else
        private const string BASE_URL = "https://crm.jdsservicesprod.com/";
#endif
        private const string AUTHORIZATION = "Basic Y3JtdXNlcjpjcm1wQDU1dzByZA==";

        public Dictionary<string, DateTime> levelsDurations;
        private string userId = "";

        public override void RegistrationCompleted(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.RegisteringTo, DataType.Game, DataType.Label }))
                return;

            JSONObject jobject = new JSONObject();

            jobject.AddField("user", ab[DataType.RegisteringTo].ToString());
            jobject.AddField("device_id", SystemInfo.deviceUniqueIdentifier);
            //jobject.AddField("game", ab[DataType.Game].ToString());
            jobject.AddField("game", DynamicValues.Instance.GetGameId());
            Debug.Log("Registering to GameID: " + jobject["game"]);
            Debug.Log("Dynamic Values ID + Registering to GameID: " + DynamicValues.Instance.GetGameId());
            

            jobject.AddField("bundle_code", VersionTrackerFromFile.Instance.GetBuildNumber());
            jobject.AddField("bundle_version", VersionTrackerFromFile.Instance.GetBundleVersion());
            jobject.AddField("origin", ab[DataType.Label].ToString());

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("Authorization", AUTHORIZATION);
            byte[] body = Encoding.UTF8.GetBytes(jobject.ToString());

            WWW apiRequest = new WWW(BASE_URL + "register/create", body, headers);
        }
        
        public override void PurchaseSuccess(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.ItemName, DataType.ItemID, DataType.Currency, DataType.ItemAmount, DataType.Game, DataType.PaymentMethod, DataType.ItemCategory }))
                return;

            float itemAmount = 0;
            Single.TryParse(ab[DataType.ItemAmount].ToString(), out itemAmount);

            JSONObject jobject = new JSONObject();

            jobject.AddField("user", userId);
            jobject.AddField("device_id", SystemInfo.deviceUniqueIdentifier);
            jobject.AddField("game", ab[DataType.Game].ToString());
            jobject.AddField("payment_method", ab[DataType.PaymentMethod].ToString());
            jobject.AddField("product_id", ab[DataType.ItemID].ToString());
            jobject.AddField("description", ab[DataType.ItemName].ToString());
            jobject.AddField("amount", itemAmount);
            jobject.AddField("currency", ab[DataType.Currency].ToString());
            jobject.AddField("bundle_code", VersionTrackerFromFile.Instance.GetBuildNumber());
            jobject.AddField("bundle_version", VersionTrackerFromFile.Instance.GetBundleVersion());

            string type = "";

            if (ab[DataType.ItemCategory].ToString() == "Consumable" || ab[DataType.ItemCategory].ToString() == "NonConsumable")
                type = "IAP";
            else
                type = ab[DataType.ItemCategory].ToString();

            jobject.AddField("type", type);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("Authorization", AUTHORIZATION);
            byte[] body = Encoding.UTF8.GetBytes(jobject.ToString());

            WWW apiRequest = new WWW(BASE_URL + "buy/create", body, headers);
        }
        
        public override void SessionStart(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] { DataType.Game }))
                return;

            JSONObject jobject = new JSONObject();

            jobject.AddField("user", userId);
            jobject.AddField("device_id", SystemInfo.deviceUniqueIdentifier);
            jobject.AddField("game", ab[DataType.Game].ToString());

            Debug.LogError("GameID: " + jobject["game"]);

            jobject.AddField("bundle_id", Application.identifier.ToString());
#if UNITY_EDITOR
            jobject.AddField("platform", "UnityEditor");
#else
            jobject.AddField("platform", Application.platform.ToString());
#endif
            jobject.AddField("bundle_code", VersionTrackerFromFile.Instance.GetBuildNumber());
            jobject.AddField("bundle_version", VersionTrackerFromFile.Instance.GetBundleVersion());

            //-----------------------------------------------------------------------------

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("Authorization", AUTHORIZATION);
            byte[] body = Encoding.UTF8.GetBytes(jobject.ToString());

            Debug.Log("Sending (SessionStart) JSON:" + jobject.ToString());

            Widow.DynamicValuesPlugin.WebConnector.MakeCall(BASE_URL + "session/create", serverResponse, body, headers);
            //WWW apiRequest = new WWW(BASE_URL + "session/create", body, headers);

            Debug.Log("Sending Start Event Request a: "+ BASE_URL + "session/create");
            Debug.Log("Body: " + jobject.ToString());
            Debug.Log("Body: " + headers.ToString());
            //StartCoroutine(DataResult(apiRequest));
        }
        public void serverResponse(string response)
        {
            Debug.LogError("Server Response to Start Session: " + response);
        }
        public override void AdEvent(AnalyticsBuilder ab)
        {
            if (!ab.assert(new[] {DataType.AdConclusion, DataType.Game }))
                return;

            JSONObject jobject = new JSONObject();

            jobject.AddField("user", userId);
            jobject.AddField("device_id", SystemInfo.deviceUniqueIdentifier);
            jobject.AddField("game", ab[DataType.Game].ToString());
            jobject.AddField("category", ab[DataType.AdConclusion].ToString());
            jobject.AddField("platform", Application.platform.ToString());
            jobject.AddField("bundle_id", Application.identifier.ToString());
            jobject.AddField("bundle_code", VersionTrackerFromFile.Instance.GetBuildNumber());
            jobject.AddField("bundle_version", VersionTrackerFromFile.Instance.GetBundleVersion());

            if (ab[DataType.AdConclusion].ToString() == "Mostrar")
                jobject.AddField("impressions", 1);
            else
                jobject.AddField("impressions", 0);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");
            headers.Add("Authorization", AUTHORIZATION);
            byte[] body = Encoding.UTF8.GetBytes(jobject.ToString());

            WWW apiRequest = new WWW(BASE_URL + "promotion/create", body, headers);
        }

        public override void SetUserId(string UserId)
        {
            userId = UserId;
        }

        private IEnumerator DataResult(WWW apiRequest)
        {
            yield return apiRequest;
            APIResult result = new APIResult(apiRequest);
        }

    }

}