namespace Widow.Analytics
{
    public class AnalyticsType
    {
        public virtual void AppInstall(AnalyticsBuilder ab) { }
        public virtual void AppLaunch(AnalyticsBuilder ab) { }

        public virtual void RegistrationCompleted(AnalyticsBuilder ab) { }
        public virtual void RegistrationFail(AnalyticsBuilder ab) { }

        public virtual void TutorialCompleted(AnalyticsBuilder ab) { }
        public virtual void LevelStarted(AnalyticsBuilder ab) { }
        public virtual void LevelRestarted(AnalyticsBuilder ab) { }
        public virtual void LevelExited(AnalyticsBuilder ab) { }
        public virtual void LevelLost(AnalyticsBuilder ab) { }
        public virtual void LevelCompleted(AnalyticsBuilder ab) { }
        public virtual void LevelDuration(AnalyticsBuilder ab) { }

        public virtual void PurchaseStart(AnalyticsBuilder ab) { }
        public virtual void PurchaseSuccess(AnalyticsBuilder ab) { }
        public virtual void PurchaseFail(AnalyticsBuilder ab) { }

        public virtual void SocialInvite(AnalyticsBuilder ab) { }
        public virtual void SocialRequest(AnalyticsBuilder ab) { }

        public virtual void SessionDuration(AnalyticsBuilder ab) { }
        public virtual void SessionStart(AnalyticsBuilder ab) { }
        public virtual void SessionEnd(AnalyticsBuilder ab) { }

        public virtual void Error(AnalyticsBuilder ab) { }

        public virtual void ButtonClicked(AnalyticsBuilder ab) { }

        public virtual void ItemUsed(AnalyticsBuilder ab) { }

        public virtual void CurrencyChanged(AnalyticsBuilder ab) { }

        public virtual void AdEvent(AnalyticsBuilder ab) { }

        public virtual void LogScreen(AnalyticsBuilder ab) { }

        public virtual void CustomEvent(string eventName, int count, string[] data = null) { }

        public virtual void SetUserId(string userId) { }
    }
}