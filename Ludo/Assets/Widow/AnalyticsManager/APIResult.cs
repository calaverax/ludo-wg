using System.Collections;
using System.Text;
using UnityEngine;
using System;
using System.Text.RegularExpressions;

namespace Widow.Analytics { 

    public class APIResult {
		public int StatusCode { get; set;}
		public JSONObject Body { get; set;}

		public APIResult(WWW www) {
			if (string.IsNullOrEmpty (www.error)) {
				this.StatusCode = 200;
			} else 
				try {this.StatusCode = Int32.Parse(Regex.Match(www.error, @"[0-9]{3}").Value);}
				catch { this.StatusCode = 500;
                    Debug.LogError(www.error.ToString());
                }

            this.Body = new JSONObject(www.text);
            Debug.LogError("API Result body: " + Body);
        }


	}
}