using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using Widow.Analytics;
using UnityEngine.SceneManagement;
using Widow.DynamicValuesPlugin;

public class AnalyticsManager : MonoBehaviour {

    #region instance properties
    public delegate void OnAnalyticsManagerInstanceCreatedAction ();
    /// <summary>
    /// Launched whenever the audio manager gets instantiated.
    /// </summary>
    public static event OnAnalyticsManagerInstanceCreatedAction onAnalyticsManagerInstanceCreated;

    private static AnalyticsManager _instance;
    public static AnalyticsManager instance {
        get {
            if (_instance == null) {
                //Find the audio manager instance.
                _instance = GameObject.FindObjectOfType<AnalyticsManager>();
                //Tells unity not to destroy this object when loading a new scene.
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }
    #endregion

    #region general properties
    private string internalButtonClick;
    [HideInInspector]
    public string buttonClick {
        set {
            AnalyticsManager.instance.internalButtonClick = value;
        }
    }
    #endregion

    #region unity methods
    void Awake () {


        if (_instance == null) {
            //If this is the first instance, makes it the singleton.
            _instance = this;
            //Tells unity not to destroy this object when loading a new scene.
            DontDestroyOnLoad(this);
            Debug.Log("Analytics manager instance created.");
            OnAnalyticsManagerInstanceCreatedInternalEvent();
        } else if (this != _instance) {
            //If a Singleton already exists and you find another reference in scene, destroy it.
            Destroy(this.gameObject);
            Debug.Log("There is already another instance of analytics manager.");
        }
        Analytics.Init();

        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }
    #endregion

    #region instance events
    private void OnAnalyticsManagerInstanceCreatedInternalEvent () {
        if (onAnalyticsManagerInstanceCreated != null)
            onAnalyticsManagerInstanceCreated();
    }
    #endregion

    #region business methods
    /// <summary>
    /// Track any real money transaction in-game.
    /// </summary>
    /// <param name="currency">Currency code in ISO 4217 format. (e.g. USD).</param>
    /// <param name="amount">Amount in cents (int). (e.g. 99).</param>
    /// <param name="itemType">Item Type bought. (e.g. Gold Pack).</param>
    /// <param name="itemId">Item bought. (e.g. 1000 gold).</param>
    /// <param name="cartType">Cart type.</param>
    public void SendBusinessEvent (string currency, int amount, string itemType, string itemId) {
        SendBusinessEvent(currency, amount, itemType, itemId, null);
    }

    /// <summary>
    /// Track any real money transaction in-game.
    /// </summary>
    /// <param name="currency">Currency code in ISO 4217 format. (e.g. USD).</param>
    /// <param name="amount">Amount in cents (int). (e.g. 99).</param>
    /// <param name="itemType">Item Type bought. (e.g. Gold Pack).</param>
    /// <param name="itemId">Item bought. (e.g. 1000 gold).</param>
    /// <param name="cartType">Cart type.</param>
    /// <param name="receipt">Transaction receipt string.</param>
    public void SendBusinessEvent (string currency, int amount, string itemType, string itemId, string receipt) {
        SendBusinessEvent(currency, amount, itemType, itemId, receipt, null);
    }

    /// <summary>
    /// Track any real money transaction in-game.
    /// </summary>
    /// <param name="currency">Currency code in ISO 4217 format. (e.g. USD).</param>
    /// <param name="amount">Amount in cents (int). (e.g. 99).</param>
    /// <param name="itemType">Item Type bought. (e.g. Gold Pack).</param>
    /// <param name="itemId">Item bought. (e.g. 1000 gold).</param>
    /// <param name="cartType">Cart type.</param>
    /// <param name="receipt">Transaction receipt string.</param>
    /// <param name="signature">Signature of transaction.</param>
    public void SendBusinessEvent (string currency, int amount, string itemType, string itemId, string receipt, string signature) {
#if UNITY_ANDROID
        Debug.Log("Log compra currency: " + currency);
        Debug.Log("Log compra amount: " + amount);
        Debug.Log("Log compra itemType: " + itemType);
        Debug.Log("Log compra itemId: " + itemId);
        Debug.Log("Log compra receipt: " + receipt);
        Debug.Log("Log compra signature: " + signature);
#if USE_GAME_ANALYTICS
        GameAnalytics.NewBusinessEventGooglePlay (currency, amount, itemType, itemId, internalButtonClick, receipt, signature);
#endif
#elif UNITY_IOS && USE_GAME_ANALYTICS
        if (string.IsNullOrEmpty (receipt)) {
			GameAnalytics.NewBusinessEventIOS (currency, amount, itemType, itemId, internalButtonClick, receipt);
		} else {
			GameAnalytics.NewBusinessEventIOSAutoFetchReceipt (currency, amount, itemType, itemId, internalButtonClick);
		}
#endif
    }
    #endregion

    /*
    void OnLevelWasLoaded (int level) {
        Analytics.LogScreen(new AnalyticsBuilder()
                .Add(DataType.Screen, SceneManager.GetActiveScene().name));
    }
    */
    private void SceneManager_sceneLoaded (Scene arg0, LoadSceneMode arg1) {
        Analytics.LogScreen(new AnalyticsBuilder()
                .Add(DataType.Screen, SceneManager.GetActiveScene().name));
    }

    void Start () {
        //   Analytics.SessionStart(new AnalyticsBuilder().Add(DataType.Game, DynamicValues.Instance.GetGameId()));

        if (LoginManager.instance != null) {
            if (LoginManager.instance.currentLoggedUser != null) {
                SetUserId(LoginManager.instance.currentLoggedUser.id);
            }
        }
#if UNITY_EDITOR && STAGE
        if (LoginManager.instance.currentLoggedUser == null)
            SetUserId("5a60be7d938368fe09e0b736");
#endif

        Analytics.SessionStart(new AnalyticsBuilder().Add(DataType.Game, JatulModule.BugsBunnyManager.GetGameID()));

    }

    void OnApplicationPause (bool paused) {
        if (paused) {
            Analytics.LogScreen(new AnalyticsBuilder()
                .Add(DataType.Screen, "Enter Background"));
        } else {
            Analytics.LogScreen(new AnalyticsBuilder()
                    .Add(DataType.Screen, SceneManager.GetActiveScene().name));
        }
    }


    void OnApplicationQuit () {
        //Analytics.LogScreen(new AnalyticsBuilder().Add(DataType.Screen, "Quit"));
        Analytics.SessionEnd(new AnalyticsBuilder().Add(DataType.Game, DynamicValues.Instance.GetGameId()).Add(DataType.Date, DateTime.Now));
        //Analytics.SessionEnd(new AnalyticsBuilder().Add(DataType.Game, JatulModule.BugsBunnyManager.GetGameID()).Add(DataType.Date, DateTime.Now));
        //Client.EndSession();
    }

    public void SetUserId (string userId) {
        GameAnalytics.SetFacebookId(userId);
        Analytics.SetUserId(userId);
    }

    public void SendDesignEvents (string screen, string popUp, string button, float value = 0) {

        string segment = screen + ":" + popUp + ":" + button;

        GameAnalytics.NewDesignEvent(segment, value);

    }

    public void SendCustomEvent (string eventName, int count, string[] data = null) {
        Analytics.CustomEvent(eventName, count, data);
    }

    public void RegistrationComplete() {
        AnalyticsBuilder ab = new AnalyticsBuilder();
        ab.Add(DataType.RegisteringTo, LoginManager.instance.currentLoggedUser.id);
        ab.Add(DataType.Game, DynamicValues.Instance.GetGameId());
        ab.Add(DataType.Label, DateTime.Now);
        Analytics.RegistrationCompleted(ab);
    }
}