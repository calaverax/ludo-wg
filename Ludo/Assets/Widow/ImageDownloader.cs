﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageDownloader : MonoBehaviour {

    public delegate void OnDownloadFinishDelegate ();
    public event OnDownloadFinishDelegate OnDownloadFinish;

    public static void GetImageByUrl (string imageURL, Image ImageCallBack, OnDownloadFinishDelegate OnFinishEvent = null, bool saveImage = false, string path = "") {

        if (string.IsNullOrEmpty(imageURL))
            imageURL = "http://juegosdesiempre.com/img/usr-default.png";
        else if (imageURL.Contains("image"))
            imageURL = imageURL.Replace("images", "img");


        ImageDownloader downloader = new GameObject().AddComponent<ImageDownloader>();
        downloader.getImageByUrl(imageURL, ImageCallBack, saveImage, path);
        if (OnFinishEvent != null)
            downloader.OnDownloadFinish += OnFinishEvent;
    }

    public static void GetImageByUrl (string imageURL, System.Action<Texture2D> CallBack, OnDownloadFinishDelegate OnFinishEvent = null) {

        if (string.IsNullOrEmpty(imageURL))
            imageURL = "http://juegosdesiempre.com/img/usr-default.png";
        else if (imageURL.Contains("image"))
            imageURL = imageURL.Replace("images", "img");

        ImageDownloader downloader = new GameObject().AddComponent<ImageDownloader>();
        downloader.getImageByUrl(imageURL, CallBack);
        if (OnFinishEvent != null)
            downloader.OnDownloadFinish += OnFinishEvent;
    }

    protected void getImageByUrl (string imageURL, Image ImageCallBack, bool saveImage = false, string path = "") {
        StartCoroutine(DownloadImage(imageURL, ImageCallBack, saveImage, path));
    }

    protected void getImageByUrl (string imageURL, System.Action<Texture2D> CallBack) {
        StartCoroutine(DownloadImage(imageURL, CallBack));
    }

    protected void OnDownloadFinished () {
        if (OnDownloadFinish != null) {
            OnDownloadFinish();
        }
        Destroy(gameObject);
    }

    protected void SaveImage (string path, Image profileImg, Texture2D texture) {
#if !UNITY_WEBPLAYER
        string savePath = System.Environment.SpecialFolder.Desktop + "BatallaFan/Ganadores/Sarlanga";

        try {
            byte[] ImageByteArray = texture.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, ImageByteArray);
        } catch (System.Exception ex) {
            Debug.LogError("Image couldn't save. Error: " + ex.Message);
        }
#endif
    }

    IEnumerator DownloadImage (string url, Image ImageCallBack, bool saveImage = false, string path = "") {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        if (www.isDone && www.progress == 1 && (www.error == "" || www.error == null)) {
            if (www.texture == null) {
                Debug.LogError("Texture is null.");
            } else {
                texture = www.texture;
                if (ImageCallBack == null) {
                    Debug.LogError("Callback is null.");
                    Destroy(gameObject);
                } else {
                    ImageCallBack.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                    ImageCallBack.color = Color.white;
                }
            }
        } else {
            Debug.Log("IsDone: " + www.isDone.ToString());
            Debug.Log("Progress: " + www.progress.ToString());
            Debug.Log("Error is empty: " + (www.error == "").ToString());
            Debug.Log("Error is null: " + (www.error == null).ToString());
        }

        OnDownloadFinished();

        if (saveImage)
            SaveImage(path, ImageCallBack, texture);

    }

    IEnumerator DownloadImage (string url, System.Action<Texture2D> CallBack) {
        WWW www = new WWW(url);
        yield return www;
        CallBack(www.texture);
        OnDownloadFinished();
    }
}

