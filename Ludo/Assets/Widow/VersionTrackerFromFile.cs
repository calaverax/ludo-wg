﻿using UnityEngine;
using System.Collections;
using System.IO;

public class VersionTrackerFromFile : Singleton<VersionTrackerFromFile>
{
    private string VERSION_NUMBER = "0";
    private string BUNDLE_VERSION = "0";
    bool systemReady = false; 
    private void Awake()
    {
        if (!systemReady)
            LoadVersionFile();

    }
    private void LoadVersionFile()
    {
        TextAsset versionsFile = (TextAsset)Resources.Load<TextAsset>("versions");

        if (versionsFile == null)
            return;

        string[] lines = versionsFile.ToString().Split('\n');

        BUNDLE_VERSION = lines[1].Split('=')[1];
        VERSION_NUMBER = lines[2].Split('=')[1];

        BUNDLE_VERSION = BUNDLE_VERSION.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
        VERSION_NUMBER = VERSION_NUMBER.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
    }

    public string GetBundleVersion() { if (systemReady) { return BUNDLE_VERSION; } else { LoadVersionFile(); return BUNDLE_VERSION; } }
    public string GetBuildNumber() { if (systemReady) { return VERSION_NUMBER; } else { LoadVersionFile(); return VERSION_NUMBER; } }

    /// <summary>
    /// DO NOT USE! Never use a New Singleton.
    /// </summary>
    protected VersionTrackerFromFile()
    { }

}
