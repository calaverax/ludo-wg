﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Widow.DynamicValuesPlugin
{
    public class WebConnector : MonoBehaviour
    {

        public class WWWData
        {
            public string url;
            public byte[] postData;
            public Dictionary<string, string> headers;

            public Action<string> onResponse;
            public bool called;
            public string response;
        }

        private Action<string> OnResponse;

        private static WebConnector instance;

        private static List<WWWData> WWWQueue = new List<WWWData>();

        private void Awake()
        {
            if (instance)
            {
                DestroyImmediate(gameObject);
                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public static WebConnector Get()
        {
            return instance;
        }

        void Update()
        {
            foreach (WWWData data in WWWQueue)
            {
                if (!data.called)
                    StartCoroutine(GetNewDataCall(data));
            }
        }

        public static void MakeCall(string url, Action<string> onResponse = null, byte[] postData = null, Dictionary<string, string> headers = null)
        {
            WWWData data = new WWWData
            {
                url = url,
                onResponse = onResponse,
                postData = postData,
                headers = headers,
                called = false,
                response = "",
            };
            //Debug.Log("url: " + url);
            //foreach (byte b in postData)
            //{
            //    Debug.Log("postData: " + b);
            //}
            //foreach (KeyValuePair<string, string> pair in headers)
            //{
            //    Debug.Log("headers: " + pair.Key + " | " + pair.Value);
            //}
            WWWQueue.Add(data);
        }

        protected IEnumerator GetNewDataCall(WWWData data)
        {
            string test = System.Text.Encoding.UTF8.GetString(data.postData);
            WWW request = new WWW(data.url, data.postData, data.headers);
            data.called = true;

            yield return request;

            if (String.IsNullOrEmpty(request.error))
            {
                data.response = request.text;
                if (data.onResponse != null)
                    data.onResponse(request.text);
            }
        }
    }
}