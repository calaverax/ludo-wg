﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
#if UNITY_EDITOR
using UnityEditor;

public class DynamicValuesMenuAddon
{
    [MenuItem("Tools/Clear PlayerPrefs")]
    private static void NewMenuOption()
    {
        PlayerPrefs.DeleteAll();
    }
}
#endif

namespace Widow.DynamicValuesPlugin
{

    public class DynamicValues : Singleton<DynamicValues>
    {
        public delegate void OnDynamicValuesInstanceCreatedAction();
        public event OnDynamicValuesInstanceCreatedAction onDynamicValuesInstanceCreated;

        public delegate void OnAdMediatorDataArrived();
        public event OnAdMediatorDataArrived onAdMediatorDataArrived;
        
        protected Dictionary<string, string> values;

        // Default jSon - STAGE_Android Freemium.
        //private string stageDictionary = "{\"gameId\":\"5939643a9204c7375ba3ad44\",\"dictionary\":{\"showAcessoAoClubeButton\":true},\"mediator\":{\"videos\":[{\"placementId\":\"Tries\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":120,\"requests\":1},\"initialAd\":{\"mode\":2,\"seconds\":120,\"requests\":1,\"sessions\":0,\"genericKey\":\"tries\",\"genericValue\":3},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vza440daf5aefa48fda1\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"CompleteLevel\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":20,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vzc315b7e3b7294d25a3\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}}";

#if STAGE //STAGE
        private const string BASE_URL = "https://www.jdsservicesstage.com/games/";
#if UNITY_ANDROID
#if PREMIUM_BUILD
        private string stageDictionary = "{\"gameId\":\"5a7b17afd1acfec6097ffc14\",\"dictionary\":{\"showAcessoAoClubeButton\":true},\"mediator\":{\"videos\":[{\"placementId\":\"Interstitial\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":120,\"requests\":1},\"initialAd\":{\"mode\":2,\"seconds\":120,\"requests\":1,\"sessions\":0,\"genericKey\":\"InterstitialVideo\",\"genericValue\":3},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vza440daf5aefa48fda1\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"CompleteLevel\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":20,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vzc315b7e3b7294d25a3\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}}";
#else
        private string stageDictionary = "{\"gameId\":\"5a7b17afd1acfec6097ffc14\",\"dictionary\":{\"showAcessoAoClubeButton\":true},\"mediator\":{\"videos\":[{\"placementId\":\"InterstitialVideo\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":120,\"requests\":1},\"initialAd\":{\"mode\":2,\"seconds\":120,\"requests\":1,\"sessions\":0,\"genericKey\":\"InterstitialVideo\",\"genericValue\":3},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vz9891e21e83574261b6\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"CompleteLevel\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":20,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vze8167c7156f442e1ac\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}}";
#endif
#elif UNITY_IOS
        private string stageDictionary = "{\"gameId\":\"5a7b17afd1acfec6097ffc14\",\"dictionary\":{\"test\":true},\"mediator\":{\"videos\":[{\"placementId\":\"Interstitial\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":120,\"requests\":1},\"initialAd\":{\"mode\":2,\"seconds\":120,\"requests\":1,\"sessions\":0,\"genericKey\":\"InterstitialVideo\",\"genericValue\":3},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vza440daf5aefa48fda1\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"CompleteLevel\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":20,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vzc315b7e3b7294d25a3\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}}";
#endif
#else //PROD
        private const string BASE_URL = "https://games.jdsservicesprod.com/";
#if UNITY_ANDROID
#if PREMIUM_BUILD
        private string stageDictionary = "{\"gameId\":\"5a7b180035ff80060944f0ca\",\"dictionary\":{\"showAcessoAoClubeButton\":true},\"mediator\":{\"videos\":[{\"placementId\":\"Interstitial\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":120,\"requests\":1},\"initialAd\":{\"mode\":2,\"seconds\":120,\"requests\":1,\"sessions\":0,\"genericKey\":\"InterstitialVideo\",\"genericValue\":3},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vza440daf5aefa48fda1\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"CompleteLevel\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":20,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vzc315b7e3b7294d25a3\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}}";
#else
        private string stageDictionary = "{\"gameId\":\"5a7b180035ff80060944f0ca\",\"dictionary\":{\"showAcessoAoClubeButton\":true},\"mediator\":{\"videos\":[{\"placementId\":\"Interstitial\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":120,\"requests\":1},\"initialAd\":{\"mode\":2,\"seconds\":120,\"requests\":1,\"sessions\":0,\"genericKey\":\"InterstitialVideo\",\"genericValue\":3},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vz9891e21e83574261b6\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"CompleteLevel\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":20,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vze8167c7156f442e1ac\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}}";
#endif
#elif UNITY_IOS
        private string stageDictionary = "{\"gameId\":\"5a7b180035ff80060944f0ca\",\"dictionary\":{\"test\":true},\"mediator\":{\"videos\":[{\"placementId\":\"Interstitial\",\"show\":true,\"frequencyCap\":{\"sessionReset\":false,\"mode\":1,\"seconds\":120,\"requests\":1},\"initialAd\":{\"mode\":2,\"seconds\":120,\"requests\":1,\"sessions\":0,\"genericKey\":\"InterstitialVideo\",\"genericValue\":3},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vza440daf5aefa48fda1\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"video\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"}]}],\"banners\":[{\"placementId\":\"Banner\",\"show\":false,\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"VideoPlacementId\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"VideoPlacementId\"}]}],\"videoRewards\":[{\"placementId\":\"CompleteLevel\",\"show\":true,\"frequencyCap\":{\"dailyLimit\":20,\"timeBetweenAds\":1},\"adNetworkPriority\":[{\"adNetwork\":\"AdColony\",\"placementId\":\"vzc315b7e3b7294d25a3\"},{\"adNetwork\":\"Vungle\",\"placementId\":\"5928459ed5a80b8a15000011\"},{\"adNetwork\":\"UnityAds\",\"placementId\":\"rewardedVideo\"}]}]}}";
#endif
#endif

        private const string GAME_SERVICE = "game/";
        private const string BUILD_DATA_SERVICE = "builddata";
        private const string APPLICATION_ID_KEY = "applicationId";
        private const string OS_KEY = "os";
        private const string BUILD_KEY = "buildCode";
        private const string DICTIONARY_KEY = "dictionary";
        private const string GAMEID_KEY = "gameId";
        private const string NOT_FOUND_KEY = "not found";

        private string BUNDLE_IDENTIFIER;
        private string BUNDLE_VERSION_CODE;
        private string BUILD_NUMBER;
        private string OS_ID;

        private bool dataAvailable = false;
        private bool _isInitialized = false;
        public bool isInitialized { get { return _isInitialized; } }
        protected string ResponseData;
        private string gameId = "";
        private JSONObject jsonDictionary;

        protected DynamicValues() { }

        protected bool initializated = false;

        #region Initialization

        public void Initialize()
        {
            JSONObject defaultJson;

            string dataToParse = string.Empty;
            dataToParse = PlayerPrefs.GetString("DynamicValuesLastValues");

            if (dataToParse.Equals(string.Empty))
                dataToParse = stageDictionary;

            defaultJson = new JSONObject(dataToParse);

            PlayerPrefs.SetString("DynamicValuesLastValues", dataToParse);
            PlayerPrefs.Save();

            if (defaultJson.keys.Contains("gameId"))
                gameId = defaultJson["gameId"].ToString();

            jsonDictionary = defaultJson["dictionary"];

            dataAvailable = true;

            BUNDLE_IDENTIFIER = Application.identifier;
            BUNDLE_VERSION_CODE = VersionTrackerFromFile.Instance.GetBundleVersion();
            BUILD_NUMBER = VersionTrackerFromFile.Instance.GetBuildNumber();

#if UNITY_ANDROID
            OS_ID = "Android";
#elif UNITY_IOS
            OS_ID = "iOS";
#endif
            if (Application.internetReachability != UnityEngine.NetworkReachability.NotReachable)
                GetValuesFromJatul();

            initializated = true;
        }
#endregion // Initialization

#region FetchingData

        private void GetValuesFromJatul()
        {
            values = new Dictionary<string, string>();

            JSONObject json = new JSONObject();

            json.AddField(APPLICATION_ID_KEY, BUNDLE_IDENTIFIER);

            json.AddField(OS_KEY, OS_ID);
            json.AddField(BUILD_KEY, BUILD_NUMBER);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");

            WebConnector.MakeCall(BASE_URL + GAME_SERVICE + BUILD_DATA_SERVICE, ParseDataJatul, Encoding.UTF8.GetBytes(json.ToString()), headers);
        }

        protected virtual void ParseDataJatul(string response)
        {
            Debug.LogError("RESPONSE: " + response);

            if (string.IsNullOrEmpty(response))
            {
                return;
            }

            JSONObject json = new JSONObject(response);

            PlayerPrefs.SetString("DynamicValuesLastValues", json.ToString());

            //Debug.Log("json.ToString(): " + json.ToString());

            if (json != null && json.keys != null && json.keys.Contains("gameId"))
                gameId = json["gameId"].ToStringUnquoted();

            if (!json["dictionary"].ToStringUnquoted().Contains(NOT_FOUND_KEY))
            {
                jsonDictionary = json["dictionary"];
                //Debug.Log("jsonDictionary: " + jsonDictionary);
                PlayerPrefs.SetString("DynamicValuesLastValues", json.ToString());
            }
            else
            {
                //jsonDictionary = json["dictionary"];
                Debug.Log("Dictionary Not Found");
            }

            if (!json["mediator"].ToStringUnquoted().Contains(NOT_FOUND_KEY))
            {
                PlayerPrefs.SetString("widowAdMediatorConfigurationJson", json["mediator"].ToString());
                if (onAdMediatorDataArrived != null)
                    onAdMediatorDataArrived.Invoke();
            }
            //Debug.Log("JSon From Jatul: " + json.ToString());
            PlayerPrefs.Save();
            DataArrived();
        }

        void DataArrived()
        {
            if (onDynamicValuesInstanceCreated != null)
                onDynamicValuesInstanceCreated();
            dataAvailable = true;
        }

#endregion // FetchingData

#region DataGetters

        public string GetGameId()
        {
            return this.gameId;
        }

        public string GetString(string id)
        {
            if (jsonDictionary.keys != null && jsonDictionary.keys.Contains(id))
                return FixJsonString(jsonDictionary[id].ToString());

            Debug.LogError("DynamicValues MISSING KEY for id: " + id);
            return "";
        }

        public JSONObject GetJson(string id)
        {
            if (jsonDictionary.keys.Contains(id))
            {
                Debug.Log(jsonDictionary.ToString());
                return jsonDictionary[id];
            }
            return null;
        }

        public int GetInt(string id)
        {
            if (jsonDictionary.keys.Contains(id))
            {
                return int.Parse(jsonDictionary[id].ToString());
            }

            Debug.LogError("DynamicValues MISSING KEY for id: " + id);
            return 0;
        }

        public float GetFloat(string id)
        {
            if (jsonDictionary.keys.Contains(id))
            {
                return float.Parse(jsonDictionary[id].ToString());
            }

            Debug.LogError("DynamicValues MISSING KEY for id: " + id);
            return 0;
        }

        public bool GetBool(string id)
        {
            if (jsonDictionary.keys != null)
            {

                if (jsonDictionary.keys.Contains(id))
                    return GetInt(id) != 0;
            }

            Debug.LogError("DynamicValues MISSING KEY for id: " + id);
            return false;
        }
#endregion

#region MiscMethods
        public bool IsAvailable()
        {
            return dataAvailable;
        }

        public static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }


#endregion

    }
}