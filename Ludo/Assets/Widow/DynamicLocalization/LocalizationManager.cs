﻿//#define USE_LOCAL_LANG
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Widow.DynamicValuesPlugin;

public class LocalizationManager : Singleton<LocalizationManager>
{
    public string CURRENT_LANG = "";

    public delegate void OnLanguageLoaded();
    public event OnLanguageLoaded onLanguageLoaded;

    public bool ForceLocalLang = false;
    public bool ForcePortuguese = false;
    public bool ForceSpanish = false;

    //public UnityEvent OnLanguageLoaded;

    private Dictionary<string, string> _localizedText;
    private bool _isReady = false; public bool isReady { get { return _isReady; } }
    private string _missingTextString = "String.Empty";

    public void StartUp()
    {
        CURRENT_LANG = Application.systemLanguage.ToString();

        if (CURRENT_LANG != "English" && CURRENT_LANG != "Spanish" && CURRENT_LANG != "Portuguese")
            CURRENT_LANG = "Spanish";

        if (ForcePortuguese)
            CURRENT_LANG = "Portuguese";

        if (ForceSpanish)
            CURRENT_LANG = "Spanish";
    }

    public void LoadLocalization(bool remote)
    {
        Debug.Log("se llamo al loadloicalization");
        if (ForceLocalLang)
        {
            LoadLocalLocalization(CURRENT_LANG);
        }
        else
        {
            if (remote)
                LoadRemoteLocalization(CURRENT_LANG);
            else
                LoadLocalLocalization(CURRENT_LANG);
        }
    }

    private void LoadRemoteLocalization(string language)
    {
        Debug.Log("se llamo al loadremotelocalization");
        _localizedText = new Dictionary<string, string>();

        string spanishDict = DynamicValues.Instance.GetString(language);
        LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(spanishDict);

        for (int i = 0; i < loadedData.items.Length; i++)
        {
            _localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
        }

        _isReady = true;

        if (onLanguageLoaded != null)
            onLanguageLoaded.Invoke();
    }

    private void LoadLocalLocalization(string fileName)
    {
        Debug.Log("LOAD LOCALIZATION MANAGER");
        _localizedText = new Dictionary<string, string>();

        TextAsset targetFile = (TextAsset)Resources.Load("Lang/"+fileName, typeof(TextAsset));

        if (targetFile != null)
        {
            if (!string.IsNullOrEmpty(targetFile.text))
            {
                string dataAsJson = targetFile.text;

                LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

                for (int i = 0; i < loadedData.items.Length; i++)
                {
                    _localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
                }
            }
        }
        else
        {
            Debug.LogError("Cannot find File");
        }

        _isReady = true;

        if (onLanguageLoaded != null)
            onLanguageLoaded.Invoke();
    }

    public string GetLocalizedValue(string key)
    {
        string result = _missingTextString;

        _localizedText.TryGetValue(key, out result);
        Debug.Log("missing key: " + key);

        return string.IsNullOrEmpty(result) ? "Missing_Key" + key: result;
    }
}