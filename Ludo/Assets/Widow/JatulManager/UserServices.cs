﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace JatulModule {
	public class UserServices : MonoBehaviour {

		#region constants
#if STAGE
        public string BASE_URL = "https://www.jdsservicesstage.com/users/";
#else
        public string BASE_URL = "https://users.jdsservicesprod.com/";
#endif

        public const string AUTH_SERVICE = "auth/";
		public const string AUTH_FACEBOOK_SERVICE = "auth/facebook/";
		public const string CREATE_SERVICE = "user/create/";
		public const string UPDATE_SERVICE = "user/update/";
		public const string DELETE_SERVICE = "user/destroy/";
		public const string FIND_SERVICE = "user/";
		public const string RESET_SERVICE = "user/resetpassword";
		public const string CREATE_SUBSCRIPTION = "subscription/create";
		public const string HAS_ACTIVE_SUBSCRIPTION = "user/hasactivesubscription?userId=";
        public const string CURRENT_SUBSCRIPTION = "user/currentsubscriptions?userId=";
        #endregion

        #region public properties
        public delegate void OnAuthSuccessfullDelegate (User user, string code);
		public event OnAuthSuccessfullDelegate onAuthSuccessfull;

		public delegate void OnAuthFailDelegate (string error);
		public event OnAuthFailDelegate onAuthFail;

		public delegate void OnCreateSuccessfullDelegate (User user, string code);
		public event OnCreateSuccessfullDelegate onCreateSuccessfull;

		public delegate void OnCreateFailDelegate (string error);
		public event OnCreateFailDelegate onCreateFail;

		public delegate void OnUpdateSuccessfullDelegate (User User);
		public event OnUpdateSuccessfullDelegate onUpdateSuccessfull;

		public delegate void OnUpdateFailDelegate (string error);
		public event OnUpdateFailDelegate onUpdateFail;

		public delegate void OnDeleteSuccessfullDelegate ();
		public event OnDeleteSuccessfullDelegate onDeleteSuccessfull;

		public delegate void OnDeleteFailDelegate (string error);
		public event OnDeleteFailDelegate onDeleteFail;

		public delegate void OnFindSuccessfullDelegate (User user);
		public event OnFindSuccessfullDelegate onFindSuccessfull;

		public delegate void OnFindFailDelegate (string error, string id);
		public event OnFindFailDelegate onFindFail;

		public delegate void OnResetPasswordFailDelegate (string error);
		public event OnResetPasswordFailDelegate onResetPasswordFail;

		public delegate void OnResetPasswordSuccessDelegate (string message);
		public event OnResetPasswordSuccessDelegate onResetPasswordSuccess;

		public delegate void OnCreateSubscriptionFailDelegate (string error);
		public event OnCreateSubscriptionFailDelegate onCreateSubscriptionFail;

		public delegate void OnCreateSubscriptionSuccessDelegate ();
		public event OnCreateSubscriptionSuccessDelegate onCreateSubscriptionSuccess;

		public delegate void OnHasActiveSubscriptonFailDelegate (string error);
		public event OnHasActiveSubscriptonFailDelegate onHasActiveSubscriptionFail;

		public delegate void OnHasActiveSubscriptionSuccessDelegate (bool hasActiveSubscription);
		public event OnHasActiveSubscriptionSuccessDelegate onHasActiveSubscriptionSuccess;

        public delegate void OnCurrentSubscriptionFailDelegate(string error);
        public event OnCurrentSubscriptionFailDelegate onCurrentSubscriptionFail;

        public delegate void OnCurrentSubscriptionSuccessDelegate(List<Subscription> currentSubscription);
        public event OnCurrentSubscriptionSuccessDelegate onCurrentSubscriptionSuccess;
        #endregion

		#region private properties
		protected Dictionary<UserServicesCalls, Action<string, byte[]>> autoCalls;
		protected Dictionary<UserServicesCalls, Action<string>> errorCallbacks;
		#endregion

		#region unity methods
		void Awake () {
			#if STAGE
			//if (PlayerPrefs.HasKey(ServerSelectionController.SERVER_SELECTION_USER_URL_SELECTED) && PlayerPrefs.GetInt (ServerSelectionController.SERVER_SELECTION_USER_URL_SELECTED) == 1) {
				BASE_URL = "https://www.jdsservicesstage.com/users/";
            //} else {
            #else
				BASE_URL = "https://users.jdsservicesprod.com/";
			//}
            #endif

			this.autoCalls = new Dictionary<UserServicesCalls, Action<string, byte[]>> ();
			this.autoCalls.Add (UserServicesCalls.AuthCall, (string url, byte[] postData) => { StartCoroutine (AuthCall (url, postData)); });
			this.autoCalls.Add (UserServicesCalls.CreateCall, (string url, byte[] postData) => { StartCoroutine (CreateCall (url, postData)); });
			this.autoCalls.Add (UserServicesCalls.UpdateCall, (string url, byte[] postData) => { StartCoroutine (UpdateCall (url, postData)); });
			this.autoCalls.Add (UserServicesCalls.DeleteCall, (string url, byte[] postData) => { StartCoroutine (DeleteCall (url, postData)); });
			this.autoCalls.Add (UserServicesCalls.FindCall, (string url, byte[] postData) => { StartCoroutine (FindCall (null, url, postData)); });
			this.autoCalls.Add (UserServicesCalls.ResetPasswordCall, (string url, byte[] postData) => { StartCoroutine (ResetPasswordCall (url, postData)); });
			this.autoCalls.Add (UserServicesCalls.CreateSubsctiptionCall, (string url, byte[] postData) => { StartCoroutine (CreateSubscriptionCall (url, postData)); });
			this.autoCalls.Add (UserServicesCalls.HasActiveSubscriptionCall, (string url, byte[] postData) => { StartCoroutine (HasActiveSubscriptionCall (url, postData)); });
			this.autoCalls.Add (UserServicesCalls.CurrentSubscriptionCall, (string url, byte[] postData) => { StartCoroutine (CurrentSubscriptionCall (url, postData)); });

			this.errorCallbacks = new Dictionary<UserServicesCalls, Action<string>> ();
			this.errorCallbacks.Add (UserServicesCalls.AuthCall, (string error) => { if (this.onAuthFail != null) this.onAuthFail (error); });
			this.errorCallbacks.Add (UserServicesCalls.CreateCall, (string error) => { if (this.onCreateFail != null) this.onCreateFail (error); });
			this.errorCallbacks.Add (UserServicesCalls.UpdateCall, (string error) => { if (this.onUpdateFail != null) this.onUpdateFail (error); });
			this.errorCallbacks.Add (UserServicesCalls.DeleteCall, (string error) => { if (this.onDeleteFail != null) this.onDeleteFail (error); });
			this.errorCallbacks.Add (UserServicesCalls.FindCall, (string error) => { if (this.onFindFail != null) this.onFindFail (error, null); });
			this.errorCallbacks.Add (UserServicesCalls.ResetPasswordCall, (string error) => { if (this.onResetPasswordFail != null) this.onResetPasswordFail (error); });
			this.errorCallbacks.Add (UserServicesCalls.CreateSubsctiptionCall, (string error) => { if (this.onCreateSubscriptionFail != null) this.onCreateSubscriptionFail (error); });
			this.errorCallbacks.Add (UserServicesCalls.HasActiveSubscriptionCall, (string error) => { if (this.onHasActiveSubscriptionFail != null) this.onHasActiveSubscriptionFail (error); });
			this.errorCallbacks.Add (UserServicesCalls.CurrentSubscriptionCall, (string error) => { if (this.onCurrentSubscriptionFail != null) this.onCurrentSubscriptionFail (error); });
		}
#endregion

#region public methods
        public void Auth(string email, string password) {
			JSONObject json = new JSONObject ();
			json.AddField ("email", email);
			json.AddField ("password", password);
			this.autoCalls [UserServicesCalls.AuthCall] (BASE_URL + AUTH_SERVICE, Encoding.UTF8.GetBytes (json.ToString ()));
		}

		public void AuthFacebook(string fbId, string fbAccessToken, string email, string nickName) {
			JSONObject json = new JSONObject ();
			json.AddField ("fb_id", fbId);
			json.AddField ("fb_access_token", fbAccessToken);
			json.AddField ("email", email);
            json.AddField ("role", "Player");
            json.AddField ("profilePicturePath", "https://graph.facebook.com/" + fbId + "/picture?width=179&height=222");
            json.AddField ("thumbnailPicturePath", "https://graph.facebook.com/" + fbId + "/picture?width=80&height=80");
            json.AddField ("nickName", nickName);
            json.AddField ("ipAddress", Network.player.externalIP);
            Debug.Log("Auth Facebook: "+ json.ToString());
			this.autoCalls [UserServicesCalls.AuthCall] (BASE_URL + AUTH_FACEBOOK_SERVICE, Encoding.UTF8.GetBytes (json.ToString ()));
		}

		public void CreateUser(User user) {
			this.autoCalls [UserServicesCalls.CreateCall] (BASE_URL + CREATE_SERVICE, Encoding.UTF8.GetBytes (user.ConvertUserToJson ().ToString ()));
		}

		public void UpdateUser(User user) {
			JSONObject json = new JSONObject ();
			json.AddField ("user", user.ConvertUserToJson ());
			this.autoCalls [UserServicesCalls.CreateCall] (BASE_URL + UPDATE_SERVICE, Encoding.UTF8.GetBytes (json.ToString ()));
		}

		public void DeleteUser(User user) {
			JSONObject json = new JSONObject ();
			json.AddField ("user", user.ConvertUserToJson ());
			this.autoCalls [UserServicesCalls.DeleteCall] (BASE_URL + DELETE_SERVICE, Encoding.UTF8.GetBytes (json.ToString ()));
		}

		public void FindUser(string id) {
			StartCoroutine (FindCall (id, BASE_URL + FIND_SERVICE + id, null));
		}
		
		public void ResetPassword(string email) {
			JSONObject json = new JSONObject ();
			json.AddField ("email", email);
			this.autoCalls [UserServicesCalls.ResetPasswordCall] (BASE_URL + RESET_SERVICE, Encoding.UTF8.GetBytes (json.ToString ()));
		}

		public void CreateSubscription (Subscription subscription) {
			this.autoCalls [UserServicesCalls.ResetPasswordCall] (BASE_URL + CREATE_SUBSCRIPTION, Encoding.UTF8.GetBytes (subscription.ConvertSubscriptionToJson().ToString()));
		}

		public void HasActiveSubscription (string id) {
			this.autoCalls [UserServicesCalls.HasActiveSubscriptionCall] (BASE_URL + HAS_ACTIVE_SUBSCRIPTION + id, null);
		}

        public void CurrentSubscription(string id) {
			this.autoCalls [UserServicesCalls.CurrentSubscriptionCall] (BASE_URL + CURRENT_SUBSCRIPTION + id, null);
        }
#endregion

#region protected methods
		protected bool ManageError (UserServicesCalls userServiceCall, string errorMessage, WWW request, string url, byte[] postData = null) {
			if (String.IsNullOrEmpty (request.error))
				return false;
			if (JatulManager.CheckExpiredToken (request.responseHeaders["STATUS"])) {
				PlayerPrefs.SetString(JatulManager.TOKEN_PREFS_KEY, JatulManager.FixJsonString((new JSONObject (request.text)) ["token"].ToString()));
				this.autoCalls [userServiceCall] (url, postData);
			} else {
				Debug.LogError (errorMessage + request.responseHeaders["STATUS"]);
				JatulManager.instance.CheckUnauthorized (request.responseHeaders["STATUS"]);
				this.errorCallbacks [userServiceCall] (request.responseHeaders["STATUS"]);
			}
			return true;
		}

		protected IEnumerator AuthCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders());
			yield return request;
			if (!this.ManageError (UserServicesCalls.AuthCall, "User Service - Auth fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
                Debug.Log("Login JSON: " + request.text);
				User user = User.CreateUserFromJson (json ["user"]);

                if (json["hasActiveSubscription"] != null)
				    user.hasActiveSubscription = bool.Parse (json ["hasActiveSubscription"].ToString ());

                if (json["oldPremiumUser"] != null)
                {
				    user.oldPremiumUserRoles = new List<string> ();
				    for (int i = 0; i < json ["oldPremiumUser"].Count; i++) {
					    user.oldPremiumUserRoles.Add (json ["oldPremiumUser"] [i].ToStringUnquoted ());
				    }
                }

				PlayerPrefs.SetString (JatulManager.TOKEN_PREFS_KEY, JatulManager.FixJsonString (json ["token"].ToString ()));
#if STAGE
				Debug.LogError ("User Service - Auth success.");
#endif
				if (this.onAuthSuccessfull != null)
					this.onAuthSuccessfull (user, JatulManager.FixJsonString (json ["code"].ToString ()));
			}
		}

		protected IEnumerator CreateCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (UserServicesCalls.CreateCall, "User Service - Create fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				User user = User.CreateUserFromJson (json ["user"]);
#if STAGE
				Debug.LogError ("User Service - Create success.");
#endif
				if (this.onCreateSuccessfull != null)
					this.onCreateSuccessfull (user, JatulManager.FixJsonString (json ["code"].ToString ()));
			}
		}

		protected IEnumerator UpdateCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (UserServicesCalls.UpdateCall, "User Service - Update fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				User user = User.CreateUserFromJson (json ["user"]);
#if STAGE
				Debug.LogError ("User Service - Update success.");
#endif
				if (this.onUpdateSuccessfull != null)
					this.onUpdateSuccessfull (user);
			}
		}

		protected IEnumerator DeleteCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (UserServicesCalls.DeleteCall, "User Service - Delete fail. Error: ", request, url, postData)) {
#if STAGE
				Debug.LogError ("User Service - Delete success.");
#endif
				if (this.onDeleteSuccessfull != null) {
					this.onDeleteSuccessfull ();
				}
			}
		}

		protected IEnumerator FindCall (string id, string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!String.IsNullOrEmpty (request.error)) {
				if (JatulManager.CheckExpiredToken (request.responseHeaders["STATUS"])) {
					PlayerPrefs.SetString(JatulManager.TOKEN_PREFS_KEY, JatulManager.FixJsonString((new JSONObject (request.text)) ["token"].ToString()));
					StartCoroutine (FindCall (id, url, postData));
					yield break;
				} else {
					Debug.LogError ("User Service - Find fail. Error: " + request.responseHeaders["STATUS"]);
					if (this.onFindFail != null)
						this.onFindFail (request.responseHeaders["STATUS"], id);
					yield break;
				}
			}
#if STAGE
			Debug.LogError ("User Service - Find success.");
#endif
			if (this.onFindSuccessfull != null)
				this.onFindSuccessfull (User.CreateUserFromJson (new JSONObject (request.text)));
		}

		protected IEnumerator ResetPasswordCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
            JSONObject json = new JSONObject(request.text);
            if (!String.IsNullOrEmpty(request.error)) {
				if (JatulManager.CheckExpiredToken (request.responseHeaders["STATUS"])) {
					PlayerPrefs.SetString (JatulManager.TOKEN_PREFS_KEY, JatulManager.FixJsonString ((new JSONObject (request.text)) ["token"].ToString ()));
					StartCoroutine (ResetPasswordCall (url, postData));
					yield break;
				} else {
					Debug.LogError ("User Service - Reset password fail. Error: " + request.responseHeaders["STATUS"]);
					if (this.onResetPasswordFail != null)
						this.onResetPasswordFail (JatulManager.FixJsonString (json ["code"].ToString ()));
					yield break;
				}
            }
#if STAGE
			Debug.LogError ("User Service - Reset password success.");
#endif
			if (this.onResetPasswordSuccess != null)
				this.onResetPasswordSuccess (JatulManager.FixJsonString(json ["msg"].ToString()));
		}

		protected IEnumerator CreateSubscriptionCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (UserServicesCalls.CreateSubsctiptionCall, "User Service - Create subscription fail. Error: ", request, url, postData)) {
#if STAGE
				Debug.LogError ("User Service - Create subscription success.");
#endif
				if (this.onCreateSubscriptionSuccess != null)
					this.onCreateSubscriptionSuccess ();
			}
		}
			
		protected IEnumerator HasActiveSubscriptionCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (UserServicesCalls.HasActiveSubscriptionCall, "User Service - Has active subscription fail. Error: ", request, url, postData)) {
#if STAGE
				Debug.LogError ("User Service - Has active subscription success.");
#endif
				if (this.onHasActiveSubscriptionSuccess != null)
					this.onHasActiveSubscriptionSuccess (bool.Parse ((new JSONObject (request.text)) ["hasActiveSubscription"].ToString ()));
			}
		}

		protected IEnumerator CurrentSubscriptionCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (UserServicesCalls.CurrentSubscriptionCall, "User Service - Current subscription fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				List<Subscription> subscriptions = new List<Subscription> ();
				for (int i = 0; i < json.Count; i++) {
					subscriptions.Add (Subscription.CreateSubscriptionFromJson (json [i]));
				}
#if STAGE
				Debug.LogError ("User Service - Current subscription success.");
#endif
				if (this.onCurrentSubscriptionSuccess != null)
					this.onCurrentSubscriptionSuccess (subscriptions);
			}
            
        }
#endregion
    }
}

public enum UserServicesCalls {
	AuthCall = 0,
	CreateCall = 1,
	UpdateCall = 2,
	DeleteCall = 3,
	FindCall = 4,
	ResetPasswordCall = 5,
	CreateSubsctiptionCall = 6,
	HasActiveSubscriptionCall = 7,
	CurrentSubscriptionCall = 8
}