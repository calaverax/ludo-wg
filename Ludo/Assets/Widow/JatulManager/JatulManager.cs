﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JatulModule {
	public class JatulManager : MonoBehaviour {

		public const string TOKEN_PREFS_KEY = "JatulToken";

		public string gameId;
		public bool IncludeUserServices;
		public bool IncludeAchievementServices;
		public bool includeTournamentServices;

		public delegate void OnUnauthorizedDelegate ();
		public event OnUnauthorizedDelegate onUnauthorized;

		private static JatulManager _instance;
		public static JatulManager instance {
			get {
				if (_instance == null) {
					_instance = FindObjectOfType<JatulManager>();
					DontDestroyOnLoad (_instance.gameObject);
				}
				return _instance;
			}
		}

		private static UserServices _userServices;
		public static UserServices userServices {
			get {
				if (!instance.IncludeUserServices)
					return null;

				if (_userServices == null)
					_userServices = instance.gameObject.AddComponent<UserServices> ();

				return _userServices;
			}
		}

		private static AchievementServices _achievementServices;
		public static AchievementServices achievementServices {
			get {
				if (!instance.IncludeAchievementServices)
					return null;

				if (_achievementServices == null)
					_achievementServices = instance.gameObject.AddComponent<AchievementServices> ();

				return _achievementServices;
			}
		}

		private static TournamentServices _tournamentServices;
		public static TournamentServices tournamentServices {
			get {
				if (!instance.includeTournamentServices)
					return null;

				if (_tournamentServices == null)
					_tournamentServices = instance.gameObject.AddComponent<TournamentServices> ();

				return _tournamentServices;
			}
		}

		public static Dictionary<string, string> GetHeaders () {
			Dictionary<string, string> headers = new Dictionary<string, string> ();
			headers.Add("Content-Type", "application/json");
			headers.Add("Authorization", string.IsNullOrEmpty(PlayerPrefs.GetString(TOKEN_PREFS_KEY)) ? "Basic Y3JtdXNlcjpjcm1wQDU1dzByZA==" : "Bearer " + PlayerPrefs.GetString(TOKEN_PREFS_KEY));
			return headers;
		}

		public static bool CheckExpiredToken (string errorCode) {
			return errorCode.Contains ("440");
		}

		public void CheckUnauthorized (string errorCode) {
			if (errorCode.Contains ("401") && this.onUnauthorized != null)
				this.onUnauthorized ();
		}

        public static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }
}
