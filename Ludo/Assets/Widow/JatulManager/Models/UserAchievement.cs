﻿using UnityEngine;
using System.Collections;

namespace JatulModule {
	public class UserAchievement {

		public string userId;
		public string achievementId;

		public static UserAchievement CreateUserAchievementFromJson(JSONObject json, bool fromFind = false) {
			UserAchievement userAchievement = new UserAchievement ();
			

            if(fromFind)
            { 
                userAchievement.userId = FixJsonString(json["user"]["id"].ToString());
                userAchievement.achievementId = json["achievement"] != null ? FixJsonString(json["achievement"]["id"].ToString()) : "";
            }
            else
            { 
                userAchievement.userId = FixJsonString(json["user"].ToString());
                userAchievement.achievementId = FixJsonString(json ["achievement"].ToString ());
            }

            return userAchievement;
		}

		public JSONObject ConvertUserAchievementToJson () {
			JSONObject json = new JSONObject ();
			json.AddField ("user", userId);
			json.AddField ("achievement", achievementId);
			return json;
		}

        private static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }
}