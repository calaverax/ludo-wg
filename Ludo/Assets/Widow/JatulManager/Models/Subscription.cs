﻿using UnityEngine;
using System.Collections;

namespace JatulModule {
	public class Subscription {

		public string userId;
		public string effectiveDate;
		public string expirationDate;
		public string duration;
		public string subscriptionTypeId;
		public bool recurrent;
		public bool active;
        public string token;

		public static Subscription CreateSubscriptionFromJson (JSONObject json) {
			Subscription subscription = new Subscription ();
			subscription.userId = FixJsonString(json ["user"].ToString());
			subscription.effectiveDate = FixJsonString(json ["effectiveDate"].ToString());
			subscription.expirationDate = FixJsonString(json ["expirationDate"].ToString());
			subscription.duration = FixJsonString(json ["duration"].ToString ());
			subscription.subscriptionTypeId = FixJsonString(json ["type"].ToString ());
			subscription.recurrent = json ["recurrent"];
			subscription.active = json ["active"];
            subscription.token = json["token"] == null ? "" : FixJsonString(json ["token"].ToString());
            return subscription;
		}

		public JSONObject ConvertSubscriptionToJson () {
			JSONObject json = new JSONObject ();
			json.AddField ("user", userId);
			json.AddField ("effectiveDate", effectiveDate);
			json.AddField ("expirationDate", expirationDate);
			json.AddField ("duration", duration);
			json.AddField ("type", subscriptionTypeId);
			json.AddField ("recurrent", recurrent);
			json.AddField ("active", active);
            json.AddField ("token", token);
            return json;
		}

        private static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }
}