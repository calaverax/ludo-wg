﻿using UnityEngine;
using System.Collections;

namespace JatulModule
{
    public class Achievement
    {

        public string name;
        public string description;
        public string game;
        public string imagePath;
        public string id;

        public static Achievement CreateAchievementFromJson(JSONObject json)
        {
            Achievement achievement = new Achievement();
            achievement.name = FixJsonString(json["name"].ToString());
            achievement.description = FixJsonString(json["description"].ToString());
            achievement.game = FixJsonString(json["game"].ToString());
            achievement.imagePath = FixJsonString(json["imagePath"].ToString());
            achievement.id = FixJsonString(json["id"].ToString());
            return achievement;
        }

        /*
        public JSONObject ConvertSubscriptionToJson()
        {
            JSONObject json = new JSONObject();
            json.AddField("user", userId);
            json.AddField("effectiveDate", effectiveDate);
            json.AddField("expirationDate", expirationDate);
            json.AddField("duration", duration);
            json.AddField("type", subscriptionTypeId);
            json.AddField("recurrent", recurrent);
            json.AddField("active", active);
            json.AddField("token", token);
            return json;
        }
        */
        private static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }
}
