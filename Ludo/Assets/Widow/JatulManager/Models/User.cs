﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JatulModule {
	public class User {
		
		public string id;
		public string email;
		public string firstName;
		public string lastName;
        public string type;
		public string nickName;
		public string gender;
		public string password;
		public string profilePicturePath;
		public string thumbnailPicturePath;
		public string ipAddress;
        public string language;
		public bool hasActiveSubscription;

        public List<string> oldPremiumUserRoles = new List<string>();

		public static User CreateUserFromJson(JSONObject json) {
			User user = new User ();

            if (json["language"] != null)
                user.language = FixJsonString(json["language"].ToString());

			user.id = FixJsonString(json ["id"].ToString());
			user.email = FixJsonString(json ["email"].ToString());
			user.firstName = json["firstName"] != null ? FixJsonString(json ["firstName"].ToString()) : "";
			user.lastName = json["lastName"] != null ? FixJsonString(json ["lastName"].ToString()) : "";
            user.type = json["type"] != null ? FixJsonString(json["type"].ToString()) : "";
            user.nickName = json["nickName"] != null ? FixJsonString(json ["nickName"].ToString()) : "";
			user.gender = json["gender"] != null ? FixJsonString(json ["gender"].ToString()) : "";
			//user.password = json ["password"].ToString();
			user.profilePicturePath = json["profilePicturePath"] != null ? FixJsonString(json ["profilePicturePath"].ToString()) : "";
			user.thumbnailPicturePath = json["thumbnailPicturePath"] != null ? FixJsonString(json ["thumbnailPicturePath"].ToString()) : "";
			user.ipAddress = json["ipAddress"] != null ? FixJsonString(json ["ipAddress"].ToString()) : "";
			user.hasActiveSubscription = json["hasActiveSubscription"] != null ? bool.Parse(json["hasActiveSubscription"].ToString()) : user.hasActiveSubscription;
			user.oldPremiumUserRoles = new List<string> ();
			if (json ["oldPremiumUser"] != null) {
				for (int i = 0; i < json ["oldPremiumUser"].Count; i++) {
					user.oldPremiumUserRoles.Add (json ["oldPremiumUser"] [i].ToStringUnquoted ());
				}
			}
            return user;
		}

		public JSONObject ConvertUserToJson () {
			JSONObject json = new JSONObject ();
            json.AddField("language", language);
			json.AddField ("id", id);
			json.AddField ("email", email);
			json.AddField ("firstName", firstName);
			json.AddField ("lastName", lastName);
            json.AddField ("type", type);
            json.AddField ("nickName", nickName);
			json.AddField ("gender", gender);
			json.AddField ("password", password);
			json.AddField ("profilePicturePath", profilePicturePath);
			json.AddField ("thumbnailPicturePath", thumbnailPicturePath);
			json.AddField ("ipAddress", ipAddress);
            json.AddField ("role", "Player");
            json.AddField ("hasActiveSubscription" , hasActiveSubscription);
			JSONObject oldPremiumUserRolesJson = new JSONObject (JSONObject.Type.ARRAY);
			foreach (string flag in oldPremiumUserRoles) {
				oldPremiumUserRolesJson.Add (flag);
			}
			json.AddField ("oldPremiumUser", oldPremiumUserRolesJson);
			return json;
		}

        private static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }
}