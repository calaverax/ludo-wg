﻿using UnityEngine;
using System.Collections;

namespace JatulModule {
	public class Tournament {

		public string id;
		public string description;
		public string game;
		public string type;
		public bool auto;
		public string fromDate;
		public string toDate;
		public string createdAt;
		public string updatedAt;

		public static Tournament CreateTournamentFromJson(JSONObject json) {
			Tournament tournament = new Tournament ();
			tournament.id = FixJsonString(json ["id"].ToString ());
			tournament.description = FixJsonString(json ["description"].ToString ());
			tournament.game = FixJsonString(json ["game"].ToString ());
			tournament.type = FixJsonString(json ["type"].ToString ());
			tournament.auto = bool.Parse(json ["auto"].ToString ());
			tournament.fromDate = FixJsonString(json ["fromDate"].ToString ());
			tournament.toDate = FixJsonString(json ["toDate"].ToString ());
			tournament.createdAt = FixJsonString(json ["createdAt"].ToString ());
			tournament.updatedAt = FixJsonString(json ["updatedAt"].ToString ());
			return tournament;
		}

		public JSONObject ConvertTournamentToJson () {
			JSONObject json = new JSONObject ();
			json.AddField ("id", id);
			json.AddField ("description", description);
			json.AddField ("game", game);
			json.AddField ("type", type);
			json.AddField ("auto", auto.ToString ());
			json.AddField ("fromDate", fromDate);
			json.AddField ("toDate", toDate);
			json.AddField ("createdAt", createdAt);
			json.AddField ("updatedAt", updatedAt);
			return json;
		}

        private static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }

	public enum TournamentType {
		Monthly = 0,
		Historic = 1
	}

}