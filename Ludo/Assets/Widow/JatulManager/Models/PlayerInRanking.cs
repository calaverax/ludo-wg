﻿using UnityEngine;
using System.Collections;

namespace JatulModule {
	public class PlayerInRanking {
		public string position;
		public string accumulatedPoints;
		public User user;

		public static PlayerInRanking CreatePlayerInRankingFromJson (JSONObject json) {
			PlayerInRanking playerInRanking = new PlayerInRanking();
            playerInRanking.user = new User();
			playerInRanking.position = FixJsonString(json ["position"].ToString());
			playerInRanking.accumulatedPoints = FixJsonString(json ["accumulatedPoints"].ToString ());
			playerInRanking.user.id = FixJsonString(json ["user"].ToString());
			return playerInRanking;
		}

		public JSONObject ConvertPlayerInRankingToJson () {
			JSONObject json = new JSONObject ();
			json.AddField ("position", position);
			json.AddField ("accumulatedPoints", accumulatedPoints);
			json.AddField ("user", user.ConvertUserToJson ());
			return json;
		}

        private static string FixJsonString(string str)
        {
            string s = str.Trim();
            if (s.StartsWith("\""))
            {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\""))
            {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }
}