﻿using UnityEngine;
//using UnityEditor;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace JatulModule {
    public class BugsBunnyManager : MonoBehaviour {

        private static BugsBunnyManager _instance;
        public static BugsBunnyManager instance {
            get {
                if (_instance == null) {
                    _instance = FindObjectOfType<BugsBunnyManager>();
                    DontDestroyOnLoad(_instance.gameObject);
                }
                return _instance;
            }
        }

        #region CONSTS

#if STAGE
        private const string BASE_URL = "https://www.jdsservicesstage.com/games/";
#else
        private const string BASE_URL = "https://games.jdsservicesprod.com/";
#endif

        private const string GAME_SERVICE = "game/";
        private const string BUILD_DATA_SERVICE = "builddata";
        private const string APPLICATION_ID_KEY = "applicationId";
        private const string OS_KEY = "os";
        private const string BUILD_KEY = "buildCode";
        private const string LAST_UPDATE_KEY = "lastUpdate";
        private const string DICTIONARY_KEY = "dictionary";
        private const string GAMEID_KEY = "gameId";
#if STAGE
        private const string TEG_GAME_KEY = "5a7b17afd1acfec6097ffc14";
#else
        private const string TEG_GAME_KEY = "5a7b180035ff80060944f0ca";
#endif


        private const float UPDATETIME = 3600f;

        //Service Keys
        private string WHATSNEW_KEY;
        private string YETEM_WEB_KEY = "yetemURL";

#endregion


#region Public Properties

        public delegate void OnRetrieveSuccessDelegate(JSONObject json);
        public event OnRetrieveSuccessDelegate OnRetrieveSuccess;

        public delegate void OnWhatsNewRetrieveSuccessDelegate(JSONObject json);
        public event OnWhatsNewRetrieveSuccessDelegate OnWhatsNewRetrieveSuccess;

        public delegate void OnYetemURLRetrieveSuccessDelegate (string json);
        public event OnYetemURLRetrieveSuccessDelegate OnYetemURLRetrieveSuccess;

#endregion


#region Private Properties

        private string BUNDLE_IDENTIFIER;
        private string BUNDLE_VERSION_CODE;
        private string OS_ID;
        private string LAST_UPDATE_TIME;
        private string DATE_KEY = "SavedDate";

        private List<string> serviceKeys;
        private Dictionary<string, System.Action<string>> keyMethodsDictionary;

#endregion


#region Public Methods

        public void GetNewData() {
            JSONObject json = new JSONObject();
            json.AddField(APPLICATION_ID_KEY, BUNDLE_IDENTIFIER);
            json.AddField(OS_KEY, OS_ID);
            json.AddField(BUILD_KEY, BUNDLE_VERSION_CODE);
            json.AddField(LAST_UPDATE_KEY, LAST_UPDATE_TIME);
            StartCoroutine(GetNewDataCall(BASE_URL + GAME_SERVICE + BUILD_DATA_SERVICE, Encoding.UTF8.GetBytes(json.ToString())));
            Invoke("GetNewData", UPDATETIME);
        }

        public static string GetGameID () {
            if (PlayerPrefs.HasKey(GAMEID_KEY)) {
                return PlayerPrefs.GetString(GAMEID_KEY);
            } else {
                return TEG_GAME_KEY;
            }
        }

#endregion


#region Private Methods

        private void Initialize() {
            SetLanguage();
#if UNITY_5_6
            BUNDLE_IDENTIFIER = Application.identifier;
#else
            BUNDLE_IDENTIFIER = Application.identifier;
#endif
            //TODO MANTENER ACTUALIZADO ESTE NÚMERO SEGÚN EL BUNDLE VERSION!!!!!
            BUNDLE_VERSION_CODE = VersionTrackerFromFile.Instance.GetBundleVersion();

#if UNITY_ANDROID
            OS_ID = "android";
            //BUNDLE_VERSION_CODE = PlayerSettings.Android.bundleVersionCode.ToString();
#elif UNITY_IOS
            OS_ID = "ios";
            //BUNDLE_VERSION_CODE = PlayerSettings.iOS.buildNumber;
#endif
            if (!PlayerPrefs.HasKey(DATE_KEY)) {
                LAST_UPDATE_TIME = System.DateTime.MinValue.ToString();
                PlayerPrefs.SetString(DATE_KEY, LAST_UPDATE_TIME);
            } else {
                LAST_UPDATE_TIME = PlayerPrefs.GetString(DATE_KEY);
            }

            InitializeKeysDictionary();

        }

        private void InitializeKeysDictionary() {
            serviceKeys = new List<string>();
            keyMethodsDictionary = new Dictionary<string, System.Action<string>>();

            serviceKeys.Add(WHATSNEW_KEY);
            serviceKeys.Add(YETEM_WEB_KEY);
            
            keyMethodsDictionary.Add(WHATSNEW_KEY, (string value) => {
                //Debug.LogError(value.Replace("\\n", System.Environment.NewLine));
                OnWhatsNewRetrieveSuccess(new JSONObject(FixJsonString(DeleteEscapeKeys(value.Replace("\\n", System.Environment.NewLine)))));
            });

            keyMethodsDictionary.Add(YETEM_WEB_KEY, (string value) => {
                OnYetemURLRetrieveSuccess(FixJsonString(value));
            });
        }

        private void SetLanguage() {
            switch (Application.systemLanguage) {
                case SystemLanguage.Portuguese:
                    WHATSNEW_KEY = "whatsNewPT";
                    break;
                case SystemLanguage.Spanish:
#if WAR_STRATEGY
                    WHATSNEW_KEY = "whatsNewESWS";
#else
                    WHATSNEW_KEY = "whatsNewES";
#endif
                    break;
                case SystemLanguage.English:
                default:
                    WHATSNEW_KEY = "whatsNewEN";
                    break;
            }
        }

        private void SaveGameID (JSONObject json) {
            //Debug.LogError(FixJsonString(json[GAMEID_KEY].ToString()));
            PlayerPrefs.SetString(GAMEID_KEY, FixJsonString(json[GAMEID_KEY].ToString()));
        }

        protected IEnumerator GetNewDataCall (string url, byte[] postData = null) {
            WWW request = new WWW(url, postData, JatulManager.GetHeaders());
            yield return request;

            Debug.LogError(request.text);
            JSONObject jsonDictionary = new JSONObject(request.text);
            if (jsonDictionary.ToString() != "null")
            { 
                SaveGameID(jsonDictionary);

                //GET DICTIONARY KEY VALUE In JSON
                jsonDictionary = jsonDictionary[DICTIONARY_KEY];
                foreach (string key in serviceKeys) {
                    if (jsonDictionary.HasField(key) && keyMethodsDictionary[key] != null) {
                        keyMethodsDictionary[key](jsonDictionary.GetField(key).ToString());
                    }
                }
                LAST_UPDATE_TIME = System.DateTime.Now.ToString();
                PlayerPrefs.SetString(DATE_KEY, LAST_UPDATE_TIME);
            }
        }      

#endregion


#region Unity Methods

        void Awake() {
            if (_instance != this && _instance != null) {
                Destroy(this.gameObject);
            }

            Initialize();
        }

#endregion

        public string DeleteEscapeKeys(string str) {
            return str.Replace("\\", "");
        }

        public static string FixJsonString(string str) {
            string s = str.Trim();
            if (s.StartsWith("\"")) {
                s = s.Remove(0, 1);
            }
            if (s.EndsWith("\"")) {
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }
    }
}
