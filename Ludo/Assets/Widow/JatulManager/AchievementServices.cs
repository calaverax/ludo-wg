﻿ using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace JatulModule {
	public class AchievementServices : MonoBehaviour {

		#region constants
#if STAGE
        public string BASE_URL = "https://www.jdsservicesstage.com/achievements/";
#else
        public string BASE_URL = "https://achievements.jdsservicesprod.com/";
#endif

        public const string CREATE_SERVICE = "userachievement/create/";
		public const string FIND_SERVICE = "userachievement/find";
        public const string FIND_ACHIVEMENTS_SERVICE = "achievement/find?game=teg&limit=0";
		#endregion

		#region public properties
		public delegate void OnAchievementCreatedDelegate (UserAchievement userAchievement);
		public event OnAchievementCreatedDelegate onAchievementCreated;

		public delegate void OnAchievementCreateFailDelegate (string error);
		public event OnAchievementCreateFailDelegate onAchievementCreateFail;

        public delegate void OnGetAchievementsDelegate(List<Achievement> achievements);
        public event OnGetAchievementsDelegate onGetAchievements;

        public delegate void OnGetAchievementsFailDelegate(string error);
        public event OnGetAchievementsFailDelegate onGetAchievementsFail;

        public delegate void OnAchievementsFoundDelegate (List<UserAchievement> userAchievements);
		public event OnAchievementsFoundDelegate onAchievementsFound;

		public delegate void OnAchievementFindFailDelegate (string error);
		public event OnAchievementFindFailDelegate onAchievementFindFail;
		#endregion

		#region private properties
		protected Dictionary<AchievementServicesCalls, Action<string, byte[]>> autoCalls;
		protected Dictionary<AchievementServicesCalls, Action<string>> errorCallbacks;
		#endregion

		#region unity methods
		void Awake () {
			#if STAGE
			//if (PlayerPrefs.HasKey(ServerSelectionController.SERVER_SELECTION_ACHIEVEMENTS_URL_SELECTED) && PlayerPrefs.GetInt (ServerSelectionController.SERVER_SELECTION_ACHIEVEMENTS_URL_SELECTED) == 1) {
            BASE_URL = "https://www.jdsservicesstage.com/achievements/";
#else
            //} else {
				BASE_URL = "https://achievements.jdsservicesprod.com/";
			//}
			#endif

			this.autoCalls = new Dictionary<AchievementServicesCalls, Action<string, byte[]>> ();
			this.autoCalls.Add (AchievementServicesCalls.CreateAchievementCall, (string url, byte[] postData) => { StartCoroutine (CreateAchievementCall (url, postData)); });
			this.autoCalls.Add (AchievementServicesCalls.GetAchievementCall, (string url, byte[] postData) => { StartCoroutine (GetAchievementsCall (url, postData)); });
			this.autoCalls.Add (AchievementServicesCalls.FindAchievementCall, (string url, byte[] postData) => { StartCoroutine (FindAchievementCall (url, postData)); });

			this.errorCallbacks = new Dictionary<AchievementServicesCalls, Action<string>> ();
			this.errorCallbacks.Add (AchievementServicesCalls.CreateAchievementCall, (string error) => { if (this.onAchievementCreateFail != null) this.onAchievementCreateFail (error); });
			this.errorCallbacks.Add (AchievementServicesCalls.GetAchievementCall, (string error) => { if (this.onGetAchievementsFail != null) this.onGetAchievementsFail (error); });
			this.errorCallbacks.Add (AchievementServicesCalls.FindAchievementCall, (string error) => { if (this.onAchievementFindFail != null) this.onAchievementFindFail (error); });
		}
		#endregion

		#region public methods
		public void CreateAchievement(UserAchievement userAchievement) {
			StartCoroutine (CreateAchievementCall (BASE_URL + CREATE_SERVICE, Encoding.UTF8.GetBytes(userAchievement.ConvertUserAchievementToJson().ToString())));
		}

        public void GetAchievements()
        {
            StartCoroutine(GetAchievementsCall(BASE_URL + FIND_ACHIVEMENTS_SERVICE, null));
        }

		public void FindAchievements(string userId, List<string> achievements) {
			JSONObject jsonArray = new JSONObject (JSONObject.Type.ARRAY);
			foreach (string achievementId in achievements) {
				jsonArray.Add (achievementId);
			}

			JSONObject json = new JSONObject (JSONObject.Type.OBJECT);
			json.AddField ("user", userId);
			json.AddField ("achievement", jsonArray);
			StartCoroutine (FindAchievementCall (BASE_URL + FIND_SERVICE, Encoding.UTF8.GetBytes(json.ToString())));
		}
		#endregion

		#region protected methods
		protected bool ManageError (AchievementServicesCalls achievementServicesCall, string errorMessage, WWW request, string url, byte[] postData = null) {
			if (String.IsNullOrEmpty (request.error))
				return false;
			if (JatulManager.CheckExpiredToken (request.responseHeaders["STATUS"])) {
				PlayerPrefs.SetString(JatulManager.TOKEN_PREFS_KEY, JatulManager.FixJsonString((new JSONObject (request.text)) ["token"].ToString()));
				this.autoCalls [achievementServicesCall] (url, postData);
			} else {
				Debug.LogError (errorMessage + request.responseHeaders["STATUS"]);
				JatulManager.instance.CheckUnauthorized (request.responseHeaders["STATUS"]);
				this.errorCallbacks [achievementServicesCall] (request.responseHeaders["STATUS"]);
			}
			return true;
		}

		protected IEnumerator CreateAchievementCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (AchievementServicesCalls.CreateAchievementCall, "Achievement Service - Create achievement success.", request, url, postData)) {
				#if STAGE
				Debug.LogError ("Achievement Service - Create achievement success.");
				#endif
				if (this.onAchievementCreated != null)
					this.onAchievementCreated (UserAchievement.CreateUserAchievementFromJson (new JSONObject (request.text)));
			}
		}

		protected IEnumerator GetAchievementsCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (AchievementServicesCalls.FindAchievementCall, "Achievement Service - Get achievemets fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				List<Achievement> achievements = new List<Achievement> ();
				for (int i = 0; i < json.Count; i++) {
					achievements.Add (Achievement.CreateAchievementFromJson (json [i]));
				}
				#if STAGE
				Debug.LogError ("Achievement Service - Get achievements success.");
				#endif
				if (this.onGetAchievements != null)
					this.onGetAchievements (achievements);
			}
        }

		protected IEnumerator FindAchievementCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (AchievementServicesCalls.GetAchievementCall, "Achievement Service - Find achievement fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				List<UserAchievement> userAchievements = new List<UserAchievement> ();

				for (int i = 0; i < json.Count; i++) {
					userAchievements.Add (UserAchievement.CreateUserAchievementFromJson (json [i], true));
				}
				#if STAGE
				Debug.LogError ("Achievement Service - Find achievement success.");
				#endif
				if (this.onAchievementsFound != null)
					this.onAchievementsFound (userAchievements);
			}
		}
		#endregion
	}
}

public enum AchievementServicesCalls {
	CreateAchievementCall = 0,
	GetAchievementCall = 1,
	FindAchievementCall = 2
}