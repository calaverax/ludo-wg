﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace JatulModule {
	public class TournamentServices : MonoBehaviour {
		
		#region constants

#if STAGE
        public string BASE_URL = "https://www.jdsservicesstage.com/tournaments/";
#else
        public string BASE_URL = "https://tournaments.jdsservicesprod.com/";
#endif

        public const string SAVE_POINTS_SERVICE = "userpoint/create/";
		public const string USER_TOURNAMENT_RANKING_SERVICE = "user/ranking?";
		public const string GET_TOURNAMENT_SERVICE = "tournament/";
		public const string GET_ACTIVE_TOURNAMENTS_SERVICE = "tournament/getcurrent?";
		public const string GET_PREVIOUS_TOURNAMENTS_SERVICE = "tournament/getprevious?";
		public const string GET_RANKING_TOP_SERVICE = "tournament/ranking?";
		#endregion

		#region properties
		public delegate void OnSavePointsSuccessfullDelegate ();
		public event OnSavePointsSuccessfullDelegate onSavePointsSuccessfull;

		public delegate void OnSavePointsFailDelegate (string error);
		public event OnSavePointsFailDelegate onSavePointsFail;

		public delegate void OnRetrieveUserTournamenRankingSuccessfullDelegate (List<PlayerInRanking> users);
		public event OnRetrieveUserTournamenRankingSuccessfullDelegate onRetrieveUserTournamenRankingSuccessfull;

		public delegate void OnRetrieveUserTournamentRankingFailDelegate (string error);
		public event OnRetrieveUserTournamentRankingFailDelegate onRetrieveUserTournamentRankingFail;

		public delegate void OnGetTournamentSuccessfullDelegate (Tournament tournament);
		public event OnGetTournamentSuccessfullDelegate onGetTournamentSuccessfull;

		public delegate void OnGetTournamentFailDelegate (string error);
		public event OnGetTournamentFailDelegate onGetTournamentFail;

		public delegate void OnGetActiveTournamentsSuccessfullDelegate (List<Tournament> tournaments);
		public event OnGetActiveTournamentsSuccessfullDelegate onGetActiveTournamentsSuccessfull;

		public delegate void OnGetActiveTournamentsFailDelegate (string error);
		public event OnGetActiveTournamentsFailDelegate onGetActiveTournamentsFail;

		public delegate void OnGetPreviousTournamentsSuccessfullDelegate (List<Tournament> tournaments);
		public event OnGetPreviousTournamentsSuccessfullDelegate onGetPreviousTournamentsSuccessfull;

		public delegate void OnGetPreviousTournamentFailDelegate (string error);
		public event OnGetPreviousTournamentFailDelegate onGetPreviousTournamentFail;

		public delegate void OnGetTournamentRankingSuccessfullDelegate (List<PlayerInRanking> playersInRanking);
		public event OnGetTournamentRankingSuccessfullDelegate onGetTournamentRankingSuccessfull;

		public delegate void OnGetTournamentRankingFailDelegate (string error);
		public event OnGetTournamentRankingFailDelegate onGetTournamentRankingFail;
		#endregion

		#region private properties
		protected Dictionary<TournamentServicesCalls, Action<string, byte[]>> autoCalls;
		protected Dictionary<TournamentServicesCalls, Action<string>> errorCallbacks;
		#endregion

		#region unity methods
		void Awake () {
			#if STAGE
			//if (PlayerPrefs.HasKey(ServerSelectionController.SERVER_SELECTION_TOURNAMENT_URL_SELECTED) && PlayerPrefs.GetInt (ServerSelectionController.SERVER_SELECTION_TOURNAMENT_URL_SELECTED) == 1) {
            BASE_URL = "https://www.jdsservicesstage.com/tournaments/";
            #else
            //} else {
				BASE_URL = "https://tournaments.jdsservicesprod.com/";
			//}
			#endif

			this.autoCalls = new Dictionary<TournamentServicesCalls, Action<string, byte[]>> ();
			this.autoCalls.Add (TournamentServicesCalls.SavePointsCall, (string url, byte[] postData) => { StartCoroutine (SavePointsCall (url, postData)); });
			this.autoCalls.Add (TournamentServicesCalls.RetrievePlayerRankingInTournamentCall, (string url, byte[] postData) => { StartCoroutine (RetrievePlayerRankingInTournamentCall (url, postData)); });
			this.autoCalls.Add (TournamentServicesCalls.GetTournamentCall, (string url, byte[] postData) => { StartCoroutine (GetTournamentCall (url, postData)); });
			this.autoCalls.Add (TournamentServicesCalls.GetActiveTournamentsCall, (string url, byte[] postData) => { StartCoroutine (GetActiveTournamentsCall (url, postData)); });
			this.autoCalls.Add (TournamentServicesCalls.GetPreviousTournamentsCall, (string url, byte[] postData) => { StartCoroutine (GetPreviousTournamentsCall (url, postData)); });
			this.autoCalls.Add (TournamentServicesCalls.GetTournamentRankingCall, (string url, byte[] postData) => { StartCoroutine (GetTournamentRankingCall (url, postData)); });

			this.errorCallbacks = new Dictionary<TournamentServicesCalls, Action<string>> ();
			this.errorCallbacks.Add (TournamentServicesCalls.SavePointsCall, (string error) => { if (this.onSavePointsFail != null) this.onSavePointsFail (error); });
			this.errorCallbacks.Add (TournamentServicesCalls.RetrievePlayerRankingInTournamentCall, (string error) => { if (this.onRetrieveUserTournamentRankingFail != null) this.onRetrieveUserTournamentRankingFail (error); });
			this.errorCallbacks.Add (TournamentServicesCalls.GetTournamentCall, (string error) => { if (this.onGetTournamentFail != null) this.onGetTournamentFail (error); });
			this.errorCallbacks.Add (TournamentServicesCalls.GetActiveTournamentsCall, (string error) => { if (this.onGetActiveTournamentsFail != null) this.onGetActiveTournamentsFail (error); });
			this.errorCallbacks.Add (TournamentServicesCalls.GetPreviousTournamentsCall, (string error) => { if (this.onGetPreviousTournamentFail != null) this.onGetPreviousTournamentFail (error); });
			this.errorCallbacks.Add (TournamentServicesCalls.GetTournamentRankingCall, (string error) => { if (this.onGetTournamentRankingFail != null) this.onGetTournamentRankingFail (error); });
		}
		#endregion

		#region public methods
		public void SavePoints (string userId, int points) {
			JSONObject json = new JSONObject();
			json.AddField("user", userId);
			json.AddField("game", JatulManager.instance.gameId);
			json.AddField("points", points);
			StartCoroutine (SavePointsCall (BASE_URL + SAVE_POINTS_SERVICE, Encoding.UTF8.GetBytes (json.ToString ())));
		}

		public void RetrievePlayerRankingInTournament (string tournamentId, string userId, int offset) {
			StartCoroutine (RetrievePlayerRankingInTournamentCall (BASE_URL + USER_TOURNAMENT_RANKING_SERVICE + "tournamentId=" + tournamentId + "&userId=" + userId + "&limit=" + offset.ToString(), null));
		}

		public void GetTournament (string tournamentId) {
			StartCoroutine (GetTournamentCall (BASE_URL + GET_TOURNAMENT_SERVICE + tournamentId, null));
		}

		public void GetActiveTournaments () {
			StartCoroutine (GetActiveTournamentsCall(BASE_URL + GET_ACTIVE_TOURNAMENTS_SERVICE + "gameId=" + JatulManager.instance.gameId, null));
		}

		public void GetPreviousTournaments (int amount, TournamentType type = TournamentType.Monthly) {
			StartCoroutine (GetTournamentCall (BASE_URL + GET_PREVIOUS_TOURNAMENTS_SERVICE + "gameId=" + JatulManager.instance.gameId + "&type=" + type.ToString() + "&limit=" + amount.ToString(), null));
		}

		public void GetTournamentRanking (string tournamentId, int page, int limit) {
			StartCoroutine (GetTournamentRankingCall (BASE_URL + GET_RANKING_TOP_SERVICE + "tournamentId=" + tournamentId + "&page=" + page.ToString() + "&limit=" + limit.ToString(), null));
		}
		#endregion

		#region protected methods
		protected bool ManageError (TournamentServicesCalls tournamentServicesCall, string errorMessage, WWW request, string url, byte[] postData = null) {
			if (String.IsNullOrEmpty (request.error))
				return false;
			if (JatulManager.CheckExpiredToken (request.responseHeaders["STATUS"])) {
				PlayerPrefs.SetString(JatulManager.TOKEN_PREFS_KEY, JatulManager.FixJsonString((new JSONObject (request.text)) ["token"].ToString()));
				this.autoCalls [tournamentServicesCall] (url, postData);
			} else {
				Debug.LogError (errorMessage + request.responseHeaders["STATUS"]);
				JatulManager.instance.CheckUnauthorized (request.responseHeaders["STATUS"]);
				this.errorCallbacks [tournamentServicesCall] (request.responseHeaders["STATUS"]);
			}
			return true;
		}

		protected IEnumerator SavePointsCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (TournamentServicesCalls.SavePointsCall, "Tournaments Servics - Save points fail. Error: ", request, url, postData)) {
				#if STAGE
				Debug.LogError ("Tournament Service - Save points success.");
				#endif
				if (this.onSavePointsSuccessfull != null)
					this.onSavePointsSuccessfull ();
			}
		}

		protected IEnumerator RetrievePlayerRankingInTournamentCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (TournamentServicesCalls.RetrievePlayerRankingInTournamentCall, "Tournament Service - Retrive player ranking in tournament fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				List<PlayerInRanking> playersInRanking = new List<PlayerInRanking> ();
				#if STAGE
				Debug.LogError ("Tournament Service - Retrieve player ranking in tournament success.");
				#endif
				for (int i = 0; i < json.Count; i++) {
					playersInRanking.Add (PlayerInRanking.CreatePlayerInRankingFromJson (json [i]));
				}
				if (this.onRetrieveUserTournamenRankingSuccessfull != null)
					this.onRetrieveUserTournamenRankingSuccessfull (playersInRanking);
			}
		}

		protected IEnumerator GetTournamentCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (TournamentServicesCalls.GetTournamentCall, "Tournament Services - Get tournament fail. Error: ", request, url, postData)) {
				#if STAGE
				Debug.LogError ("Tournament Service - Retrieve player ranking in tournament success.");
				#endif
				if (this.onGetTournamentSuccessfull != null)
					this.onGetTournamentSuccessfull (Tournament.CreateTournamentFromJson (new JSONObject (request.text)));
			}
		}

		protected IEnumerator GetActiveTournamentsCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (TournamentServicesCalls.GetActiveTournamentsCall, "Tournament service - Get active tournaments fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				List<Tournament> tournaments = new List<Tournament> ();
				for (int i = 0; i < json.Count; i++) {
					tournaments.Add (Tournament.CreateTournamentFromJson (json [i]));
				}
				#if STAGE
				Debug.LogError ("Tournament Service - Get active tournaments success.");
				#endif
				if (this.onGetActiveTournamentsSuccessfull != null)
					this.onGetActiveTournamentsSuccessfull (tournaments);
			}
		}

		protected IEnumerator GetPreviousTournamentsCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (TournamentServicesCalls.GetPreviousTournamentsCall, "Tournament Service - Get previous tournaments fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				List<Tournament> tournaments = new List<Tournament> ();
				for (int i = 0; i < json.Count; i++) {
					tournaments.Add (Tournament.CreateTournamentFromJson (json [i]));
				}
				#if STAGE
				Debug.LogError ("Tournament Service - Get previous tournaments success.");
				#endif
				if (this.onGetPreviousTournamentsSuccessfull != null)
					this.onGetPreviousTournamentsSuccessfull (tournaments);
			}
		}

		protected IEnumerator GetTournamentRankingCall (string url, byte[] postData = null) {
			WWW request = new WWW (url, postData, JatulManager.GetHeaders ());
			yield return request;
			if (!this.ManageError (TournamentServicesCalls.GetTournamentRankingCall, "Tournament Service - Get tournament ranking fail. Error: ", request, url, postData)) {
				JSONObject json = new JSONObject (request.text);
				List<PlayerInRanking> playersInRanking = new List<PlayerInRanking> ();
				for (int i = 0; i < json.Count; i++) {
					playersInRanking.Add (PlayerInRanking.CreatePlayerInRankingFromJson (json [i]));
				}
				#if STAGE
				Debug.LogError ("Tournament Service - Get tournament ranking success.");
				#endif
				if (this.onGetTournamentRankingSuccessfull != null)
					this.onGetTournamentRankingSuccessfull (playersInRanking);
			}
		}
		#endregion
	}
}

public enum TournamentServicesCalls {
	SavePointsCall = 0,
	RetrievePlayerRankingInTournamentCall = 1,
	GetTournamentCall = 2,
	GetActiveTournamentsCall = 3,
	GetPreviousTournamentsCall = 4,
	GetTournamentRankingCall = 5
}