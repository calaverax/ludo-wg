﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;
using Widow.DynamicValuesPlugin;

public class LoadMainAfterSplash : MonoBehaviour {

    bool _canLoadMainMenu = false;

    private void Start () {
        if (ConnectedToInternet()) {
            DynamicValues.Instance.onAdMediatorDataArrived += StartCallback;
            DynamicValues.Instance.onDynamicValuesInstanceCreated += StartCallback;
            DynamicValues.Instance.Initialize();
        } else {
            StartCallback();
        }
    }
    // Use this for initialization
    void StartCallback () {
        LocalizationManager.Instance.StartUp();
        LocalizationManager.Instance.onLanguageLoaded += ShowMainMenu;
        LocalizationManager.Instance.LoadLocalization(ConnectedToInternet());
    }

    void setupShowMainMenu () {
        _canLoadMainMenu = true;
        ShowMainMenu();
    }

    void ShowMainMenu () {
        if (LocalizationManager.Instance.isReady)
            SceneManager.LoadScene("Main Menu");
    }

    #region INTERNET_UTILS
    public bool ConnectedToInternet () {

#if UNITY_ANDROID || UNITY_IOS
        string HtmlText = GetHtmlFromUri("http://google.com");
        if (HtmlText == "") {
            //No connection
            return false;
        } else if (!HtmlText.Contains("schema.org/WebPage")) {
            //Redirecting since the beginning of googles html contains that 
            //phrase and it was not found
            return false;
        } else {
            //success
            return true;
        }
#elif !UNITY_WEBGL
        if (Network.player.ipAddress.ToString() != "127.0.0.1")
            return true;
#endif
        return false;
    }

    string GetHtmlFromUri (string resource) {
        string html = "";
        HttpWebRequest req = (HttpWebRequest) WebRequest.Create(resource);
        req.Timeout = 7000;
        try {
            using (HttpWebResponse resp = (HttpWebResponse) req.GetResponse()) {
                bool isSuccess = (int) resp.StatusCode < 299 && (int) resp.StatusCode >= 200;
                if (isSuccess) {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream())) {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        char[] cs = new char[80];
                        reader.Read(cs, 0, cs.Length);
                        foreach (char ch in cs) {
                            html += ch;
                        }
                    }
                }
            }
        } catch (WebException e) {
            return "timeout";
        } catch {
            return "";
        }
        return html;
    }
    #endregion INTERNET_UTILS

}
