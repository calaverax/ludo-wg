﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class SxfAndBGMController : MonoBehaviour {

    Toggle soundStatusToggle;
    float _volume = 1;

    // Use this for initialization
    void Start () {

        soundStatusToggle = GetComponent<Toggle>();

        if (soundStatusToggle != null)
            soundStatusToggle.onValueChanged.AddListener(ToggleSound);

        _volume = PlayerPrefs.GetFloat(SoundManagerScript.SOUND_KEY, 1.0f);
        soundStatusToggle.isOn = _volume == 1;

        // BGM
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "Main Menu")
            Camera.main.GetComponent<AudioSource>().volume = _volume;
    }

    public void ToggleSound (bool status) {
        
        // Toggle Audio On/Off
        _volume = status ? 1 : 0;

        Debug.Log("Sound Status: " + status + " Volume For Sound And BGM: " + _volume);

        // BGM
        Camera.main.GetComponent<AudioSource>().volume = _volume;

        // SFX
        SoundManagerScript.buttonAudioSource.volume = _volume;
        SoundManagerScript.dismissalAudioSource.volume = _volume;
        SoundManagerScript.diceAudioSource.volume = _volume;
        SoundManagerScript.winAudioSource.volume = _volume;
        SoundManagerScript.safeHouseAudioSource.volume = _volume;
        SoundManagerScript.playerAudioSource.volume = _volume;

        PlayerPrefs.SetFloat(SoundManagerScript.SOUND_KEY, _volume);
        PlayerPrefs.Save();
    }

    void OnDestroy() {

        if (soundStatusToggle != null)
            soundStatusToggle.onValueChanged.RemoveListener(ToggleSound);
    }
}
