﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SoundManagerScript : MonoBehaviour {

	public AudioClip buttonAudioClip;
	public AudioClip dismissalAudioClip;
	public AudioClip diceAudioClip;
	public AudioClip winAudioClip;
	public AudioClip safeHouseAudioClip;
	public AudioClip playerAudioClip;

	public static AudioSource buttonAudioSource;
	public static AudioSource dismissalAudioSource;
	public static AudioSource diceAudioSource;
	public static AudioSource winAudioSource;
	public static AudioSource safeHouseAudioSource;
	public static AudioSource playerAudioSource;

    public static string SOUND_KEY = "SoundsVolume";

    float _volume = 1.0f;

    AudioSource AddAudio(AudioClip clip, bool playOnAwake, bool loop, float  volume)
	{
		AudioSource audioSource = gameObject.AddComponent<AudioSource> ();
		audioSource.clip = clip;
		audioSource.playOnAwake = playOnAwake;
		audioSource.loop = loop;
		audioSource.volume = volume;
		return audioSource;
	}

    void Start () {

        _volume = PlayerPrefs.GetFloat(SOUND_KEY, 1.0f);

        // SFX
        buttonAudioSource = AddAudio(buttonAudioClip, false, false, _volume);
        dismissalAudioSource = AddAudio(dismissalAudioClip, false, false, _volume);
        diceAudioSource = AddAudio(diceAudioClip, false, false, _volume);
        winAudioSource = AddAudio(winAudioClip, false, false, _volume);
        safeHouseAudioSource = AddAudio(safeHouseAudioClip, false, false, _volume);
        playerAudioSource = AddAudio(playerAudioClip, false, false, _volume);

        // BGM
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "Main Menu")
            Camera.main.GetComponent<AudioSource>().volume = _volume;

    }
}
